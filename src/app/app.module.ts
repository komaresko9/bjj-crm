import { DevToolsExtension, NgRedux, NgReduxModule } from '@angular-redux/store'
import { ErrorHandler, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { Keyboard } from '@ionic-native/keyboard/ngx'
import { SplashScreen } from '@ionic-native/splash-screen'
import { StatusBar } from '@ionic-native/status-bar'
import { IonicStorageModule } from '@ionic/storage'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular'

import { GET_INITIAL_STATE, IState, rootReducer } from '../shared/store/store'

import { SignUpSuccessPageModule } from '../pages/start/sign-up/sign-up-success/sign-up-success.module'
import { StartPageModule } from '../pages/start/start.module'
import { TabsPageModule } from '../pages/tabs/tabs.module'
import { ComponentsModule } from '../shared/components/components.module'

import { AddClientPageModule } from '../pages/add-client/add-client.module'
import { AddClubPageModule } from '../pages/add-club/add-club.module'
import { AddGroupPageModule } from '../pages/add-group/add-group.module'
import { ChartPageModule } from '../pages/chart/chart.module'
import { HomePageModule } from '../pages/home/home.module'
import { PaymentPageModule } from '../pages/payment/payment.module'
import { PeopleListPageModule } from '../pages/people-list/people-list.module'
import { SettingsPageModule } from '../pages/settings/settings.module'

import { MyApp } from './app.component'

import { AuthService } from '../shared/services/auth.service'
import { ChannelsService } from '../shared/services/channels.service'
import { ClientsService } from '../shared/services/clients.service'
import { ClubsService } from '../shared/services/clubs.service'
import { CurrencyService } from '../shared/services/currency.service'
import { DateService } from '../shared/services/date.service'
import { GroupsService } from '../shared/services/groups.service'
import { VisitingsService } from '../shared/services/visitings.service'

import { FireProvider } from '../shared/providers/fire.provider'
import { PatternsProvider } from '../shared/providers/patterns.provider'
import { RefsProvider } from '../shared/providers/refs.provider'

import { registerLocaleData } from '@angular/common'
import locale from '@angular/common/locales/ru-UA'
import { LOCALE_ID } from '@angular/core'
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx'
import { ActionsModule } from '../shared/store/actions/actions.module';

registerLocaleData(locale)

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    ActionsModule,
    PaymentPageModule,
    StartPageModule,
    SignUpSuccessPageModule,
    TabsPageModule,
    ComponentsModule,
    AddClubPageModule,
    AddGroupPageModule,
    AddClientPageModule,
    HomePageModule,
    PeopleListPageModule,
    SettingsPageModule,
    ChartPageModule,
    NgReduxModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: LOCALE_ID, useValue: 'ru-UA'},
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseAnalytics,
    Keyboard,

    FireProvider,
    RefsProvider,
    PatternsProvider,

    AuthService,
    ClientsService,
    GroupsService,
    ChannelsService,
    CurrencyService,
    DateService,
    ClubsService,
    VisitingsService,
  ],
})
export class AppModule {
  constructor(
    ngRedux: NgRedux<IState>,
    devTools: DevToolsExtension,
  ) {

    const storeEnhancers = devTools.isEnabled() ? [devTools.enhancer()] : []

    GET_INITIAL_STATE()
    .then((state) => {
      ngRedux.configureStore(
        rootReducer,
        state,
        [],
        storeEnhancers,
        )
      })
  }
}
