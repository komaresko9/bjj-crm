import { Component, ViewChild } from '@angular/core'
import { SplashScreen } from '@ionic-native/splash-screen'
import { StatusBar } from '@ionic-native/status-bar'
import { Nav, Platform } from 'ionic-angular'

import { StartPage } from '../pages/start/start'
import { TabsPage } from '../pages/tabs/tabs'

import * as firebase from 'firebase'
import { CONFIG } from './config'
import { FIREBASE_CONFIG } from './firebase.credentials'

import { select } from '@angular-redux/store'
import { Storage } from '@ionic/storage'
import { Observable } from 'rxjs'
import { AddClientPage } from '../pages/add-client/add-client'
import { AddClubPage } from '../pages/add-club/add-club'
import { AddGroupPage } from '../pages/add-group/add-group'
import { ClubsService } from '../shared/services/clubs.service'
import { GroupsService } from '../shared/services/groups.service'

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  public rootPage // = StartPage
  @ViewChild(Nav) public nav: Nav

  @select(['auth', 'isLoggedIn']) public isLoggedIn$: Observable<boolean>
  @select(['auth', 'info', 'signOut', 'isSuccess']) public signOutIsSuccess$: Observable<boolean>

  @select(['club', 'info', 'get', 'isSuccess']) public getClubIsSuccess$: Observable<boolean>
  @select(['club', 'info', 'get', 'error']) public getClubError$: Observable<firebase.FirebaseError>
  @select(['club', 'info', 'create', 'isSuccess']) public createClubIsSuccess$: Observable<boolean>
  @select(['club', 'info', 'create', 'error']) public createClubError$: Observable<firebase.FirebaseError>

  @select(['groups', 'info', 'get', 'isSuccess']) public getGroupsIsSuccess$: Observable<boolean>
  @select(['groups', 'info', 'get', 'error']) public getGroupsError$: Observable<firebase.FirebaseError>

  @select(['clients', 'info', 'get', 'isSuccess']) public getClientsIsSuccess$: Observable<boolean>
  @select(['clients', 'info', 'get', 'error']) public getClientsError$: Observable<firebase.FirebaseError>

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public clubsService: ClubsService,
    public groupsService: GroupsService,
    public storage: Storage,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault()
      splashScreen.hide()
      if (CONFIG.DEV) {
        firebase.initializeApp(FIREBASE_CONFIG.DEV)
      } else if (CONFIG.PRODUCTION) {
        firebase.initializeApp(FIREBASE_CONFIG.PRODUCTION)
      }
      this.afterEnter()
    })
  }

  public ionViewWillLeave() {
    // remove all on sign out true
    delete this.isLoggedIn$
    delete this.signOutIsSuccess$
    delete this.getClubIsSuccess$
    delete this.getClubError$
    delete this.createClubIsSuccess$
    delete this.createClubError$
    delete this.getGroupsIsSuccess$
    delete this.getGroupsError$
    delete this.getClientsIsSuccess$
    delete this.getClientsError$
  }

  private afterEnter() {
    this.isLoggedIn$
      .subscribe((isLoggedIn) => {
        if (isLoggedIn) {
          this.getClubIsSuccess$
            .subscribe((getClubIsSuccess) => {
              if (getClubIsSuccess) {
                this.getGroupsIsSuccess$
                  .subscribe((getGroupsIsSuccess) => {
                    if (getGroupsIsSuccess) {
                      this.getClientsIsSuccess$
                        .subscribe((getClientsIsSuccess) => {
                          if (getClientsIsSuccess) {
                            this.nav.setRoot(TabsPage)
                          }
                        })
                      this.getClientsError$
                        .subscribe((getClientsError) => {
                          if (getClientsError) {
                            this.nav.setRoot(AddClientPage, { isRoot: true })
                          }
                        })
                    }
                  })
                this.getGroupsError$
                  .subscribe((getGroupsError) => {
                    if (getGroupsError) {
                      this.nav.setRoot(AddGroupPage, { isRoot: true })
                    }
                  })
              }
            })
          this.getClubError$
            .subscribe((getClubError) => {
              if (getClubError) {
                this.nav.setRoot(AddClubPage, { isRoot: true })

                this.createClubIsSuccess$
                  .subscribe((createClubIsSuccess) => {
                    if (createClubIsSuccess) {
                      this.nav.setRoot(AddGroupPage, { isRoot: true })
                    }
                  })

                this.createClubError$
                  .subscribe((createClubError) => {
                    if (createClubError) {
                      this.nav.setRoot(AddGroupPage, { isRoot: true })
                    }
                  })
              }
            })
        } else {
          this.nav.setRoot(StartPage)
        }
      })

    this.signOutIsSuccess$
      .subscribe((signOutIsSuccess) => {
        if (signOutIsSuccess) {
          this.nav.setRoot(StartPage)
        }
      })
  }
}
