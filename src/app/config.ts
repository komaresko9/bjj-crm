export interface IConfig {
    DEV: boolean,
    PRODUCTION: boolean,
}

export const CONFIG: IConfig = {
    DEV: false,
    PRODUCTION: true,
}
