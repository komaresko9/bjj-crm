import { Component } from '@angular/core'
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { NavController, ToastController } from 'ionic-angular'

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  public paymentSrc: SafeResourceUrl

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public domSanitizerService: DomSanitizer,
            ) {
          let url = 'https://send.monobank.ua/wgnXQqZD?amount=100?text=komaresko9@gmail.com'
          this.paymentSrc = this.domSanitizerService.bypassSecurityTrustResourceUrl(url)
  }
}
