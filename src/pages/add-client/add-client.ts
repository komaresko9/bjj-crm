import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { Client } from '../../shared/models/client.model'

import { ChannelsService } from '../../shared/services/channels.service'
import { ClientsService } from '../../shared/services/clients.service'
import { TabsPage } from '../tabs/tabs'

import { Observable } from 'rxjs'
import { Channel } from '../../shared/models/channel.model'
import { Group } from '../../shared/models/group.model'

import { NgRedux, select } from '@angular-redux/store'

import { ClientActions } from '../../shared/store/actions/client'
import { IState } from '../../shared/store/store'

@IonicPage()
@Component({
  selector: 'page-add-client',
  templateUrl: 'add-client.html',
})
export class AddClientPage {

  public data = {
    firstName: '',
    lastName: '',
    telephone: '+380',
    group: [],
    club: '',
    channel: '',
  }

  @select(['club', 'data', 'key']) public clubKey$: Observable<string>

  @select(['clients', 'info', 'create', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['clients', 'info', 'create', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['clients', 'info', 'create', 'isError']) public isError$: Observable<boolean>
  @select(['clients', 'info', 'create', 'isSuccess']) public isSuccess$: Observable<boolean>

  @select(['groups', 'data', 'list']) public groups$: Observable<Group[]>

  @select(['channels', 'data', 'list']) public channels$: Observable<Channel[]>

  private isRoot = false

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public clientsService: ClientsService,
              public channelsService: ChannelsService,
              private store: NgRedux<IState>,
              private actions: ClientActions,
              ) {

                this.isRoot = this.navParams.get('isRoot')

                this.groups$
                  .subscribe((list: Group[]) => {
                    if (list && list.length) {
                      this.data.group.push(list[0].key)
                    }
                  })

                this.clubKey$
                  .subscribe((key: string) => {
                      if (key) {
                        this.data.club = key
                      }
                  })

                this.channels$
                  .subscribe((list: Channel[]) => {
                    if (list && list.length) {
                      this.data.channel = list[0].key
                    } else if (list && !list.length) {
                      this.channelsService.get()
                    }
                  })

                this.isSuccess$
                  .subscribe((isSuccess) => {
                    if (isSuccess) {
                      if (this.isRoot) {
                        this.navCtrl.setRoot(TabsPage)
                      } else {
                        this.navCtrl.pop()
                      }
                    }
                  })
  }

  public ionViewWillEnter() {
    this.store.dispatch(this.actions.createReset())
  }

  public ionViewWillLeave() {
    delete this.isLoading$
    delete this.error$
    delete this.isError$
    delete this.isSuccess$
    delete this.groups$
    delete this.clubKey$
    delete this.channels$
  }

  public addClient() {
    let newClient = new Client({
      firstName: this.data.firstName,
      lastName: this.data.lastName,
      club: this.data.club,
      group: typeof this.data.group === 'string' ? [this.data.group] : this.data.group,
      telephone: this.data.telephone,
      channel: this.data.channel,
    })

    this.clientsService.create(Object.assign({}, newClient))
  }

}
