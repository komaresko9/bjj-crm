import { Component } from '@angular/core'
import { IonicPage } from 'ionic-angular'

import { ChartPage } from '../chart/chart'
import { HomePage } from '../home/home'
import { PeopleListPage } from '../people-list/people-list'
import { SettingsPage } from '../settings/settings'

interface ITab {
  icon: string,
  title: string,
  root: any,
  params?: any,
  hideOnSubPages: boolean,
}

export enum HomeConfTypes {
  visiting,
  money,
  statistics,
}

@IonicPage({
  segment: 'tabs',
})
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  public text = {
    journal: 'Журнал',
    money: 'Гроші',
    chart: 'Статистика',
    people: 'Люди',
    settings: 'Налаштування',
  }

  public tabs: ITab[] = [
    {
      icon: 'md-checkmark-circle-outline',
      title: this.text.journal,
      root: HomePage,
      params: { type: HomeConfTypes.visiting },
      hideOnSubPages: true,
    },
    {
      icon: 'md-cash',
      title: this.text.money,
      root: HomePage,
      params: { type: HomeConfTypes.money },
      hideOnSubPages: true,
    },
    {
      icon: 'md-stats',
      title: this.text.chart,
      root: HomePage,
      params: { type: HomeConfTypes.statistics },
      hideOnSubPages: true,
    },
    // {
    //   icon: 'md-stats',
    //   title: this.text.chart,
    //   root: ChartPage,
    //   hideOnSubPages: true,
    // },
    {
      icon: 'md-people',
      title: this.text.people,
      root: PeopleListPage,
      hideOnSubPages: true,
    },
    {
      icon: 'md-settings',
      title: this.text.settings,
      root: SettingsPage,
      hideOnSubPages: true,
    },
  ]
}
