import { Component, OnInit, ViewChild } from '@angular/core'
import { Storage } from '@ionic/storage'
import { IonicPage, NavController, NavParams, Select } from 'ionic-angular'
import { PopoverController } from 'ionic-angular'
import { ToastController } from 'ionic-angular'

import { Group } from '../../shared/models/group.model'
import { ISchedule } from '../../shared/models/schedule.model'
import { ClientsService } from '../../shared/services/clients.service'
import { CurrencyService } from '../../shared/services/currency.service'
import { DateService } from '../../shared/services/date.service'
import { GroupsService } from '../../shared/services/groups.service'
import { AddClientPage } from '../add-client/add-client'

import { NgRedux, select } from '@angular-redux/store'
import { Observable } from 'rxjs'
import { ChannelsService } from '../../shared/services/channels.service'
import { IState } from '../../shared/store/store'

@IonicPage()
@Component({
  selector: 'page-add-group',
  templateUrl: 'add-group.html',
})
export class AddGroupPage {

  @ViewChild(Select) public select: Select
  public selectOptions = {
    title: 'Виберіть дні тренуваннь',
  }

  public data: Group = {
    name: '',
    key: '',
    isDeleted: false,
    schedule: [],
    club: '',
    amountMonth: undefined,
    amountDay: undefined,
  }

  public daysList = this.dateService.daysList

  @select(['club', 'data', 'key']) public clubKey$: Observable<string>

  @select(['groups', 'info', 'create', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['groups', 'info', 'create', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['groups', 'info', 'create', 'isError']) public isError$: Observable<boolean>
  @select(['groups', 'info', 'create', 'isSuccess']) public createGroupsIsSuccess$: Observable<boolean>

  @select(['clients', 'info', 'create', 'isSuccess']) public createClientsIsSuccess$: Observable<boolean>

  private isRoot = false

  constructor(
              public navCtrl: NavController,
              public popoverCtrl: PopoverController,
              public toastCtrl: ToastController,
              public groupsService: GroupsService,
              public clientsService: ClientsService,
              public storage: Storage,
              public navParams: NavParams,
              public dateService: DateService,
              public currencyService: CurrencyService,
              public channelsService: ChannelsService,
              private store: NgRedux<IState>,
            ) {
              this.isRoot = this.navParams.get('isRoot')

              this.clubKey$
                .subscribe((clubKey) => {
                  if (clubKey) {
                    this.data.club = clubKey
                  }
                })
  }

  public create() {
    this.groupsService.create(this.data)
    if (this.isRoot) {
      this.channelsService.get()
    }

    this.createGroupsIsSuccess$
      .subscribe((createGroupsIsSuccess) => {
        if (createGroupsIsSuccess) {
          this.isRoot
            ? this.navCtrl.setRoot(AddClientPage, { isRoot: true })
            : this.navCtrl.pop()
        }
      })
  }

  public ionViewWillLeave() {
    delete this.isLoading$
    delete this.error$
    delete this.isError$
    delete this.createGroupsIsSuccess$
    delete this.createClientsIsSuccess$
  }

  public canCreate(): boolean {
    return !this.data.name.length
        || !this.data.schedule.length
        || !this.data.amountMonth
        || !this.data.amountDay
  }

  public addSchedule(myEvent): void {
    this.select.open(myEvent)
    this.select.registerOnChange(this.onSelect.bind(this))
  }

  public onSelect(): void {
    const schedules = this.data.schedule
    this.data.schedule = []
    if (this.select.value && (this.select.value as number[]).length) {
      this.select.value.forEach((day: number) => {
        const shcedule: ISchedule = {
          day,
          hh: '19',
          mm: '00',
        }

        const index = schedules.findIndex((schedule: ISchedule) => {
          return schedule.day === day
        })
        if (index > -1) {
          shcedule.hh = schedules[index].hh
          shcedule.mm = schedules[index].mm
        }
        this.data.schedule.push(shcedule)
      })
    }
  }

  public getScheduleTime(item): string {
    return item.hh + ':' + item.mm
  }

  public setTime(event, item: ISchedule) {
    item.hh = getNN(event.hour)
    item.mm = getNN(event.minute)
  }

}

function getNN(num) {
  return num < 10 ? '0' + num : num.toString()
}
