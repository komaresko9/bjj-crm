import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ComponentsModule } from '../../shared/components/components.module'
import { ChartPage } from './chart'

@NgModule({
  declarations: [
    ChartPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ChartPage),
  ],
})
export class ChartPageModule {}
