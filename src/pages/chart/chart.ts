import { Component, ViewChild } from '@angular/core'
import { Chart } from 'chart.js'
import { AlertController, NavController, NavParams, ToastController } from 'ionic-angular'

import { DateService } from '../../shared/services/date.service'
import { VisitingsService } from '../../shared/services/visitings.service'

@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
})
export class ChartPage {

  public m
  public y
  public selectedGroupName

  public barChart: Chart
  public doughnutChart: Chart
  public lineChart: Chart
  @ViewChild('barCanvas') public barCanvas
  @ViewChild('doughnutCanvas') public doughnutCanvas
  @ViewChild('lineCanvas') public lineCanvas

  public text = {
    balance: 'Баланс в групі',
    lineChartTitle: 'Медіана',
  }

  public labels = ['Дохід', 'Борги']

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public visitingsService: VisitingsService,
              public alert: AlertController,
              public dateService: DateService,
            ) {
  }

  public ionViewWillEnter() {
    this.y = new Date().getFullYear()
    this.m = new Date().getMonth()
    this.selectedGroupName = ''
  }

  public ionViewDidLoad() {

    const colors = {
      red: {
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgba(255,99,132,1)',
        hoverBackgroundColor: '#FF6384',
      },
      blue: {
        backgroundColor: 'rgba(54, 162, 235, 0.2)',
        borderColor: 'rgba(54, 162, 235, 1)',
        hoverBackgroundColor: '#36A2EB',
      },
      yellow: {
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        hoverBackgroundColor: '#FFCE56',
      },
      green: {
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        hoverBackgroundColor: '#FF6384',
      },
      purple: {
        backgroundColor: 'rgba(153, 102, 255, 0.2)',
        borderColor: 'rgba(153, 102, 255, 1)',
        hoverBackgroundColor: '#36A2EB',
      },
      orange: {
        backgroundColor: 'rgba(255, 159, 64, 0.2)',
        borderColor: 'rgba(255, 159, 64, 1)',
        hoverBackgroundColor: '#FFCE56',
      },
    }

    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: this.labels,
        datasets: [{
          label: `${this.text.balance} ${this.selectedGroupName}`,
          data: [12, -19],
          backgroundColor: [
            colors.green.backgroundColor,
            colors.red.backgroundColor,
          ],
          borderColor: [
            colors.green.borderColor,
            colors.red.borderColor,
          ],
          borderWidth: 1,
        }],
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
            },
          }],
        },
        onClick: this.chartOnClick,
      },
    })

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: this.labels,
        datasets: [{
          label: '# of Votes',
          data: [12, -19],
          backgroundColor: [
            colors.green.backgroundColor,
            colors.red.backgroundColor,
          ],
          borderColor: [
            colors.green.borderColor,
            colors.red.borderColor,
          ],
          // hoverBackgroundColor: [
          //   colors.green.hoverBackgroundColor,
          //   colors.red.hoverBackgroundColor,
          // ],
        }],
      },
  })

    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: this.text.lineChartTitle,
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [65, 59, 80, 81, 56, 55, 40],
            spanGaps: false,
          },
        ],
      },
    })
  }

  public back() {
    if (this.m === 0) {
      this.y -= 1
      this.m = 11
    } else {
      this.m -= 1
    }
    this.barChart.config.data.datasets[0].data = [19, -this.m]
    this.doughnutChart.config.data.datasets[0].data = [19, -this.m]
    this.updateCharts()
    // this.init(this.y, this.m)
  }

  public forward() {
    if (this.m === 11) {
      this.y += 1
      this.m = 0
    } else {
      this.m += 1
    }
    // this.init(this.y, this.m)
    this.barChart.config.data.datasets[0].data = [19, -this.m]
    this.doughnutChart.config.data.datasets[0].data = [19, -this.m]
    this.updateCharts()
  }

  public groupChanged() {
    this.barChart.config.data.datasets[0].label = `${this.text.balance} ${this.selectedGroupName}`
    this.updateCharts()
  }

  public updateCharts() {
    this.barChart.update()
    this.doughnutChart.update()
    this.lineChart.update()
  }

  public chartOnClick = (event) => {
    const activeElement = this.barChart.getElementAtEvent(event)
    if (activeElement.length) {
      console.warn(this.labels[activeElement[0]._index], this.selectedGroupName)
    }
  }
}
