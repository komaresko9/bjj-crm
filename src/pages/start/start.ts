import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { IonicPage, Slides } from 'ionic-angular'
import { startPageText } from '../../shared/translations/ua'

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage implements OnInit, OnDestroy {

  public interval
  @ViewChild(Slides) public slides: Slides

  public text: any = startPageText

  public data: any[] = [
    {
      text: this.text.slides.addClub,
    },
    {
      text: this.text.slides.addGroup,
    },
    {
      text: this.text.slides.addStudent,
    },
    {
      text: this.text.slides.addVisiting,
    },
    {
      text: this.text.slides.addMoney,
    },
    {
      text: this.text.slides.viewStatistics,
    },
    {
      text: this.text.slides.relax,
    },
  ]

  public ngOnInit() {
    this.interval = setInterval(() => {
      this.slides.slideNext(500)
    }, 8000)
  }

  public ngOnDestroy() {
    clearInterval(this.interval)
  }

}
