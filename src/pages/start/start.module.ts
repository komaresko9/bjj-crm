import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'

import { SignInPageModule } from './sign-in/sign-in.module'
import { StartPage } from './start'

@NgModule({
  declarations: [
    StartPage,
  ],
  imports: [
    SignInPageModule,
    IonicPageModule.forChild(StartPage),
  ],
})
export class StartPageModule {}
