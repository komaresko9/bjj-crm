import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ComponentsModule } from '../../../shared/components/components.module'
import { ForgotPasswordPageModule } from './forgot-password/forgot-password.module'
import { SignInPage } from './sign-in'

@NgModule({
  declarations: [
    SignInPage,
  ],
  imports: [
    ComponentsModule,
    ForgotPasswordPageModule,
    IonicPageModule.forChild(SignInPage),
  ],
})
export class SignInPageModule {}
