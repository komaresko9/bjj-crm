import { NgRedux, select } from '@angular-redux/store'
import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { Observable } from 'rxjs'
import { IAsyncBtnData } from '../../../../shared/components/async-btn/async-btn.component'
import { AuthService } from '../../../../shared/services/auth.service'
import { AuthActions } from '../../../../shared/store/actions/auth'
import { IState } from '../../../../shared/store/store'
import { forgotPasswordText } from '../../../../shared/translations/ua'
import { AuthForm } from '../../auth-form/auth-form'

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage extends AuthForm {

  public text: any = forgotPasswordText

  public form: FormGroup
  public get email() {
    return 'email'
  }

  @select(['auth', 'info', 'forgotPassword', 'isSuccess']) public isSuccess$: Observable<boolean>
  @select(['auth', 'info', 'forgotPassword', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['auth', 'info', 'forgotPassword', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['auth', 'info', 'forgotPassword', 'isError']) public isError$: Observable<boolean>

  public get asyncBtnData(): IAsyncBtnData {
    return {
      isLoading: this.isLoading$,
      text: this.text.changePassword,
      isDisabled: !this.form.valid,
    } as IAsyncBtnData
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthService,
    public formBuilder: FormBuilder,
    private store: NgRedux<IState>,
    private actions: AuthActions,
  ) {
    super()
    this.form = this.formBuilder.group({
      email: ['', [
          Validators.pattern(this.PatternsProvider.emailPattern),
          Validators.required,
          Validators.minLength(this.PatternsProvider.min),
          Validators.maxLength(this.PatternsProvider.max),
        ],
      ],
    })

    this.isSuccess$.subscribe((isSuccess) => {
      if (isSuccess) {
        this.navCtrl.pop()
      }
    })
  }

  public ionViewWillLeave() {
    delete this.isSuccess$
    delete this.isLoading$
    delete this.error$
    delete this.isError$
  }

  public onSubmit() {
    if (!this.form.valid) { return }

    let email: string = this.form.get(this.email).value

    this.store.dispatch(this.actions.forgotPasswordRequest())
    this.authService.forgotPassword(email)
  }
}
