import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ComponentsModule } from '../../../../shared/components/components.module'
import { ForgotPasswordPage } from './forgot-password'

@NgModule({
  declarations: [
    ForgotPasswordPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ForgotPasswordPage),
  ],
})
export class ForgotPasswordPageModule {}
