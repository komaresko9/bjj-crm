import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { IAsyncBtnData } from '../../../shared/components/async-btn/async-btn.component'
import { AuthService } from '../../../shared/services/auth.service'
import { ClientsService } from '../../../shared/services/clients.service'
import { ClubsService } from '../../../shared/services/clubs.service'
import { GroupsService } from '../../../shared/services/groups.service'
import { loginText } from '../../../shared/translations/ua'
import { AddClubPage } from '../../add-club/add-club'
import { AddGroupPage } from '../../add-group/add-group'
import { AuthForm } from '../auth-form/auth-form'

import { NgRedux, select } from '@angular-redux/store'
import { Observable } from 'rxjs'
import { ChannelsService } from '../../../shared/services/channels.service'
import { AuthActions } from '../../../shared/store/actions/auth'
import { IState } from '../../../shared/store/store'
import { AddClientPage } from '../../add-client/add-client'
import { TabsPage } from '../../tabs/tabs'
import { ForgotPasswordPage } from './forgot-password/forgot-password'

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage extends AuthForm {

  public form: FormGroup
  public text: any = loginText

  public get email() { return 'email' }
  public get password() { return 'password' }

  @select(['auth', 'info', 'signIn', 'isSuccess']) public isSuccess$: Observable<boolean>
  @select(['auth', 'info', 'signIn', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['auth', 'info', 'signIn', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['auth', 'info', 'signIn', 'isError']) public isError$: Observable<boolean>

  @select(['club', 'info', 'get', 'isSuccess']) public getClubIsSuccess$: Observable<boolean>
  @select(['club', 'info', 'get', 'error']) public getClubError$: Observable<firebase.FirebaseError>

  @select(['groups', 'info', 'get', 'isSuccess']) public getGroupsIsSuccess$: Observable<boolean>
  @select(['groups', 'info', 'get', 'error']) public getGroupsError$: Observable<firebase.FirebaseError>

  @select(['clients', 'info', 'get', 'isSuccess']) public getClientsIsSuccess$: Observable<boolean>
  @select(['clients', 'info', 'get', 'error']) public getClientsError$: Observable<firebase.FirebaseError>

  public get asyncBtnData(): IAsyncBtnData {
    return {
      isLoading: this.isLoading$,
      text: this.text.login,
      isDisabled: !this.form.valid,
    } as IAsyncBtnData
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthService,
    public formBuilder: FormBuilder,
    public clientsService: ClientsService,
    public clubsService: ClubsService,
    public groupsService: GroupsService,
    public channelsService: ChannelsService,
    private store: NgRedux<IState>,
    private actions: AuthActions,
  ) {
    super()
    this.form = this.formBuilder.group({
      email: ['', [
          Validators.pattern(this.PatternsProvider.emailPattern),
          Validators.required,
          Validators.minLength(this.PatternsProvider.min),
          Validators.maxLength(this.PatternsProvider.max),
        ],
      ],
      password: ['', [
          Validators.required,
          Validators.minLength(this.PatternsProvider.min),
          Validators.maxLength(this.PatternsProvider.max),
        ],
      ],
    })
  }

  public ionViewWillLeave() {
    delete this.isSuccess$
    delete this.isLoading$
    delete this.error$
    delete this.isError$

    delete this.getClubIsSuccess$
    delete this.getClubError$

    delete this.getGroupsIsSuccess$
    delete this.getGroupsError$

    delete this.getClientsIsSuccess$
    delete this.getClientsError$
  }

  public onSubmit() {
    if (!this.form.valid) {
      return
    }
    let email: string = this.form.get(this.email).value
    let pass: string = this.form.get(this.password).value

    this.authService.signIn(email, pass)
    this.isSuccess$
      .subscribe((isSuccess) => {
        if (isSuccess) {
          this.clubsService.get()

          this.getClubIsSuccess$
            .subscribe((getClubIsSuccess) => {
              if (getClubIsSuccess) {
                this.groupsService.get()

                this.getGroupsIsSuccess$
                  .subscribe((getGroupsIsSuccess) => {
                    if (getGroupsIsSuccess) {
                      this.clientsService.get()

                      this.getClientsIsSuccess$
                        .subscribe((getClientsIsSuccess) => {
                          if (getClientsIsSuccess) {
                            this.navCtrl.setRoot(TabsPage)
                          }
                        })

                      this.getClientsError$
                        .subscribe((getClientsError) => {
                          if (getClientsError) {
                            this.channelsService.get()
                            this.navCtrl.setRoot(AddClientPage, { isRoot: true })
                          }
                        })
                    }
                  })

                this.getGroupsError$
                  .subscribe((getGroupsError) => {
                    if (getGroupsError) {
                      this.navCtrl.setRoot(AddGroupPage, { isRoot: true })
                    }
                  })
              }
            })
          this.getClubError$
            .subscribe((getClubError) => {
              if (getClubError) {
                this.navCtrl.setRoot(AddClubPage, { isRoot: true })
              }
            })
        }
      })
  }

  public goToForgotPasswordPage(): void {
    this.store.dispatch(this.actions.forgotPasswordReset())
    this.navCtrl.push(ForgotPasswordPage)
  }
}
