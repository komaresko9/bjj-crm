import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ComponentsModule } from '../../../shared/components/components.module'
import { SignUpPage } from './sign-up'

@NgModule({
  declarations: [
    SignUpPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SignUpPage),
  ],
  entryComponents: [
  ],
})
export class SignUpPageModule {}
