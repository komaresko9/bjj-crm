import { NgRedux } from '@angular-redux/store'
import { Component } from '@angular/core'
import { IonicPage, NavController } from 'ionic-angular'
import { AuthActions } from '../../../../shared/store/actions/auth'
import { IState } from '../../../../shared/store/store'
import { signupSuccessText } from '../../../../shared/translations/ua'
import { SignInPage } from '../../sign-in/sign-in'
import { StartPage } from '../../start'

@IonicPage()
@Component({
  selector: 'page-sign-up-success',
  templateUrl: 'sign-up-success.html',
})
export class SignUpSuccessPage {
  public text: any = signupSuccessText

  constructor(
      public navCtrl: NavController,
      private store: NgRedux<IState>,
      private actions: AuthActions,
    ) { }

  public toSignInPage() {
    this.store.dispatch(this.actions.signUpReset())
    this.navCtrl.setRoot(StartPage)
    this.navCtrl.push(SignInPage)
  }
}
