import { select } from '@angular-redux/store'
import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { IonicPage, NavController } from 'ionic-angular'
import { Observable } from 'rxjs'
import { IAsyncBtnData } from '../../../shared/components/async-btn/async-btn.component'
import { AuthService } from '../../../shared/services/auth.service'
import { signUpText } from '../../../shared/translations/ua'
import { AuthForm } from '../auth-form/auth-form'
import { SignUpSuccessPage } from './sign-up-success/sign-up-success'

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage extends AuthForm {

  public text: any = signUpText
  public form: FormGroup
  public get email() {
    return 'email'
  }
  public get password() {
    return 'password'
  }

  @select(['auth', 'info', 'signUp', 'isSuccess']) public isSuccess$: Observable<boolean>
  @select(['auth', 'info', 'signUp', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['auth', 'info', 'signUp', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['auth', 'info', 'signUp', 'isError']) public isError$: Observable<boolean>

  public get asyncBtnData(): IAsyncBtnData {
  return {
    isLoading: this.isLoading$,
    text: this.text.signup,
    isDisabled: !this.form.valid,
    } as IAsyncBtnData
  }

  constructor(
    public navCtrl: NavController,
    public authService: AuthService,
    public formBuilder: FormBuilder,
  ) {
    super()

    this.form = this.formBuilder.group({
      email: ['', [
          Validators.pattern(this.PatternsProvider.emailPattern),
          Validators.required,
          Validators.minLength(this.PatternsProvider.min),
          Validators.maxLength(this.PatternsProvider.max),
        ],
      ],
      password: ['', [
          Validators.required,
          Validators.minLength(this.PatternsProvider.min),
          Validators.maxLength(this.PatternsProvider.max),
        ],
      ],
    })

    this.isSuccess$.subscribe((isSuccess) => {
      if (isSuccess) {
        this.navCtrl.setRoot(SignUpSuccessPage, { isRoot: true })
      }
    })
  }

  public ionViewWillLeave() {
    delete this.isSuccess$
    delete this.isLoading$
    delete this.error$
    delete this.isError$
  }

  public onSubmit() {
    if (!this.form.valid) {
      return
    }

    let email: string = this.form.get(this.email).value
    let pass: string = this.form.get(this.password).value

    this.authService.signUp(email, pass)
  }
}
