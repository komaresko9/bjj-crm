import { FormGroup } from '@angular/forms'
import { PatternsProvider } from '../../../shared/providers/patterns.provider'
import { authFormText } from '../../../shared/translations/ua'

export class AuthForm {

  public get PatternsProvider() {
    return PatternsProvider
  }

  public authFormText: any = authFormText

  public getAuthErrorMessage(error: firebase.FirebaseError) {
    if (error.code === 'auth/user-disabled') {
      return this.authFormText.blocked || error.message
    } else if (error.code === 'auth/email-already-in-use') {
      return this.authFormText.alreadyInUse || error.message
    } else if (error.code === 'auth/weak-password') {
      return this.authFormText.minLengthInvalid || error.message
    } else if (error.code === 'auth/invalid-email') {
      return this.authFormText.emailInvalid || error.message
    } else if (error.code === 'auth/wrong-password') {
      return this.authFormText.passwordIncorrect || error.message
    } else if (error.code === 'auth/user-not-found') {
      return this.authFormText.notFound || error.message
    } else if (error.code === 'auth/email-not-verified') {
      return this.authFormText.confirmEmail || error.message
    } else {
      return error.message
    }
  }

  public hasError(field: string, form: FormGroup) {
    return (
      form.get(field).hasError('minlength') ||
      form.get(field).hasError('maxlength') ||
      form.get(field).hasError('pattern') ||
      form.get(field).hasError('required')
    ) && form.get(field).touched
  }

  public required(field: string, form: FormGroup) {
    return form.get(field).hasError('required') && form.get(field).touched
  }

  public minlength(field: string, form: FormGroup) {
    return form.get(field).hasError('minlength') && form.get(field).touched
  }

  public maxlength(field: string, form: FormGroup) {
    return form.get(field).hasError('maxlength') && form.get(field).touched
  }

  public pattern(field: string, form: FormGroup) {
    return form.get(field).hasError('pattern') && form.get(field).touched
  }
}