import { select } from '@angular-redux/store'
import { Component } from '@angular/core'
import { Alert, NavController } from 'ionic-angular'
import { Observable } from 'rxjs'
import { AuthService } from '../../shared/services/auth.service'
import { PaymentPage } from '../payment/payment'
import { StartPage } from '../start/start'

import { AlertController } from 'ionic-angular'
import { ClubsService } from '../../shared/services/clubs.service'

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  public text = {
    title: 'Налаштування',
    paymentHeader: 'Керуй оплатою',
    payment: 'Оплата',
    another: 'Інше',
    exit: 'Вийти',
    confirmLogOutQuestion: 'Ви дійсно хочете вийти з облікового запису?',
    cancel: 'Cкасувати',
  }

  public alert: Alert

  @select(['auth', 'info', 'signOut', 'isSuccess']) public isSuccess$: Observable<boolean>
  @select(['auth', 'info', 'signOut', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['auth', 'info', 'signOut', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['auth', 'info', 'signOut', 'isError']) public isError$: Observable<boolean>

  constructor(
    public nav: NavController,
    private authService: AuthService,
    public clubsService: ClubsService,
    public alertCtrl: AlertController,
  ) {
    // this.isSuccess$
    // .subscribe((isSuccess) => {
    //   if (isSuccess) {
    //     this.nav.setRoot(StartPage)
    //   }
    // })
    this.alert = this.alertCtrl.create({
      title: this.text.confirmLogOutQuestion,
      buttons: [
        { text: this.text.cancel },
        {
          text: this.text.exit,
          handler: () => { this.authService.signOut() },
        },
      ],
    })
  }

  public ionViewWillLeave() {
    delete this.isSuccess$
    delete this.isLoading$
    delete this.error$
    delete this.isError$
  }

  public logOut() {
    this.alert.present()
  }

  public toPayment() {
    this.nav.push(PaymentPage)
  }

  public get clubName() {
    // TODO: FIX
    // this.clubsService.club
    return ''
            ? ''
            // ? this.clubsService.club.name
            : ''
  }
}
