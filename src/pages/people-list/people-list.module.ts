import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ComponentsModule } from '../../shared/components/components.module'
import { PeopleListPage } from './people-list'

@NgModule({
  declarations: [
    PeopleListPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(PeopleListPage),
  ],
})
export class PeopleListPageModule {}
