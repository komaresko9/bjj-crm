import { Component } from '@angular/core'
import { Keyboard } from '@ionic-native/keyboard/ngx'
import { IonicPage } from 'ionic-angular'

import { ClientsService } from '../../shared/services/clients.service'
import { GroupsService } from '../../shared/services/groups.service'

import { select } from '@angular-redux/store'
import { Observable } from 'rxjs'

// enum ShowListEnum {
//   clients,
//   groups,
// }

@IonicPage()
@Component({
  selector: 'page-people-list',
  templateUrl: 'people-list.html',
})
export class PeopleListPage {

  @select(['groups', 'data', 'selectedGroupKey']) public selectedGroupKey$: Observable<string>
  @select(['clients', 'get', 'info', 'is']) public clientsListIsLoading$: Observable<boolean>
  @select(['clients', 'data', 'mapByGroupKey']) public clientsMapByGroupKey$: Observable<any>

  // public selectedList: ShowListEnum = ShowListEnum.clients
  public filter
  public text = {
    clients: 'Студенти',
    groups: 'Групи',
    search: 'Пошук',
  }

  constructor(
    public clientsService: ClientsService,
    public groupsService: GroupsService,
    private keyboard: Keyboard,
  ) {
  }

  public ionViewWillLeave() {
    delete this.selectedGroupKey$
    delete this.clientsListIsLoading$
    delete this.clientsMapByGroupKey$
  }

  // public get ShowListEnum(): typeof ShowListEnum {
  //   return ShowListEnum
  // }

  // public get showClients(): boolean {
  //   return this.selectedList === ShowListEnum.clients
  // }
  // public get showGroups(): boolean {
  //   return this.selectedList === ShowListEnum.groups
  // }
  public hideKeyboard() {
    try {
      this.keyboard.hide()
    } catch (keyboardError) {
      return
    }
  }
}
