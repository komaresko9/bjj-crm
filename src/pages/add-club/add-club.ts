import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Storage } from '@ionic/storage'
import { IonicPage, NavController } from 'ionic-angular'
import { IAsyncBtnData } from '../../shared/components/async-btn/async-btn.component'
import { Club } from '../../shared/models/club.model'
import { AuthForm } from '../start/auth-form/auth-form'

import { select } from '@angular-redux/store'
import { Observable } from 'rxjs'

import { AuthService } from '../../shared/services/auth.service'
import { ClubsService } from '../../shared/services/clubs.service'
import { AddGroupPage } from '../add-group/add-group'

@IonicPage()
@Component({
  selector: 'page-add-club',
  templateUrl: 'add-club.html',
})
export class AddClubPage extends AuthForm {

  public text: any = {
    title: 'Створіть клуб',
    createOwnClub: 'Придумайте назву свого клубу',
    enterClubName: 'Введіть назву клубу',
    empty: 'Поле не повинне бути пустим',
    createClub: 'Створити клуб',
  }

  public data: Club = {
    name: '',
    key: '',
    user: '',
    isDeleted: false,
  }

  public form: FormGroup

  public get name() {
    return 'name'
  }

  @select(['auth', 'user', 'uid']) public uid$: Observable<string>
  @select(['club', 'info', 'create', 'isLoading']) public isLoading$: Observable<boolean>
  @select(['club', 'info', 'create', 'isSuccess']) public isSuccess$: Observable<boolean>
  @select(['club', 'info', 'create', 'error']) public error$: Observable<firebase.FirebaseError>
  @select(['club', 'info', 'create', 'isError']) public isError$: Observable<boolean>

  public get asyncBtnData(): IAsyncBtnData {
    return {
      isLoading: this.isLoading$,
      text: this.text.createClub,
      isDisabled: !this.form.valid,
    } as IAsyncBtnData
  }

  constructor(public navCtrl: NavController,
              public clubsService: ClubsService,
              public formBuilder: FormBuilder,
              public authService: AuthService,
              public storage: Storage,
            ) {
              super()
              this.form = this.formBuilder.group({
                name: ['', [
                    Validators.required,
                    Validators.minLength(this.PatternsProvider.min - 5),
                    Validators.maxLength(this.PatternsProvider.max + 10),
                  ],
                ],
              })
  }

  public ionViewWillLeave() {
    delete this.uid$
    delete this.isLoading$
    delete this.isSuccess$
    delete this.error$
    delete this.isError$
  }

  public onSubmit() {
    if (!this.form.valid) { return }

    this.data.name = this.form.get(this.name).value

    this.uid$
      .subscribe((uid) => {
        this.data.user = uid
        this.clubsService.create(this.data)
      })
  }
}
