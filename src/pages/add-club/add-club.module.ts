import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ComponentsModule } from '../../shared/components/components.module'
import { AddClubPage } from './add-club'

@NgModule({
  declarations: [
    AddClubPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AddClubPage),
  ],
})
export class AddClubPageModule {}
