import { Component } from '@angular/core'
import { Keyboard } from '@ionic-native/keyboard/ngx'
import { NavController, NavParams, ToastController } from 'ionic-angular'

import { Client } from '../../shared/models/client.model'
import { ClientsService } from '../../shared/services/clients.service'
import { ClubsService } from '../../shared/services/clubs.service'
import { DateService } from '../../shared/services/date.service'
import { VisitingsService } from '../../shared/services/visitings.service'

import { select } from '@angular-redux/store'
import { Observable } from 'rxjs'
import { ISchedule } from '../../shared/models/schedule.model'
import { HomeConfTypes } from '../tabs/tabs'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  public searchQuery: string = ''
  public clients = []
  public clientsAfterSearch = []
  public dates = []
  public schedule = []

  public y: number
  public m: number
  public names: string[] = []
  public today: number

  public text = {
    sum: 'Всього',
    search: 'Пошук',
    empty: 'Додайте першого студента в групу',
    add: 'Додати студента',
  }
  public type: HomeConfTypes

  @select(['club', 'data', 'key']) public clubKey$: Observable<string>
  @select(['groups', 'data', 'selectedGroupKey']) public selectedGroupKey$: Observable<string>
  @select(['groups', 'data', 'map']) public groupsMap$: Observable<any>
  @select(['clients', 'data', 'mapByGroupKey']) public clientsMapByGroupKey$: Observable<any>
  @select(['visitings', 'data', 'map']) public visitingsMap$: Observable<any>
  @select(['visitings', 'info', 'loadingMap']) public visitingsLoadingMap$: Observable<any>

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public clientsService: ClientsService,
              public visitingsService: VisitingsService,
              public clubsService: ClubsService,
              private keyboard: Keyboard,
              public dateService: DateService,
            ) {
  }

  public ionViewDidLoad() {
    this.type = this.navParams.data.type
    let y = new Date().getFullYear()
    let m = new Date().getMonth()

    this.init(y, m)
  }

  public ionViewWillLeave() {
    delete this.clubKey$
    delete this.selectedGroupKey$
    delete this.groupsMap$
    delete this.clientsMapByGroupKey$
    delete this.visitingsMap$
    delete this.visitingsLoadingMap$
  }

  public init(y: number, m: number) {
    this.initGroups(y, m)
  }

  public initDates(y: number, m: number) {
    if (!this.schedule) {
      return
    }
    this.dates = []

    this.y = y
    this.m = m
    let startDay = new Date(y, m, 1)

    while (startDay.getMonth() === m) {
      let index = this.schedule.indexOf(startDay.getDay())
      if (index > -1) {
        this.dates.push(new Date(startDay))
      }
      if (!this.today && (
        new Date().getDate() === startDay.getDate() || new Date().getDate() - 1 === startDay.getDate()
      )) {
        this.today = startDay.getDate()
      }
      startDay.setDate(startDay.getDate() + 1)
    }
  }

  public initGroups(y: number, m: number) {
    this.groupsMap$
      .subscribe((groupsMap) => {
        if (groupsMap) {
          this.selectedGroupKey$
            .subscribe((selectedGroupKey) => {
              if (selectedGroupKey)  {
                this.schedule = []
                groupsMap[selectedGroupKey].schedule.forEach((scheduleOfSelectedGroup: ISchedule) => {
                  this.schedule.push(scheduleOfSelectedGroup.day)
                })
                this.initDates(y, m)
                this.initVisitings()
                this.clientsMapByGroupKey$
                  .subscribe((clientsMapByGroupKey) => {
                    if (clientsMapByGroupKey) {
                      this.clients = clientsMapByGroupKey[selectedGroupKey] || []
                      this.clientsAfterSearch = clientsMapByGroupKey[selectedGroupKey] || []
                    }
                  })
              }
          })
        }
      })
  }

  public isEmpty(clientsMapByGroupKey, selectedGroupKey) {
    try {
      return !clientsMapByGroupKey[selectedGroupKey].length
    } catch (error) {
      return true
    }
  }

  public initVisitings() {
    this.visitingsService.get(this.dates)
  }

  public back() {
    if (this.m === 0) {
      this.y -= 1
      this.m = 11
    } else {
      this.m -= 1
    }
    this.init(this.y, this.m)
  }

  public forward() {
    if (this.m === 11) {
      this.y += 1
      this.m = 0
    } else {
      this.m += 1
    }
    this.init(this.y, this.m)
  }

  public goToCurrentMonth() {
    let y = new Date().getFullYear()
    let m = new Date().getMonth()
    if (this.y !== y || this.m !== m) {
      this.init(y, m)
    }
  }

  public isToday(date: Date) {
    return this.today + 1 === date.getDate() && this.m === new Date().getMonth()
  }

  public filterItems(ev: any) {
    // Reset items back to all of the items
    this.clientsAfterSearch = Object.create(this.clients)
    // set val to the value of the searchbar
    const val = ev.target.value

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.clientsAfterSearch = this.clientsAfterSearch.filter((item: Client) => {
        return ((item.firstName + item.lastName).toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    }
  }

  public hideKeyboard() {
    try {
      this.keyboard.hide()
    } catch (keyboardError) {
      return
    }
  }

  public get isMoney() {
    return this.type === HomeConfTypes.money
  }

  public get isStatistics() {
    return this.type === HomeConfTypes.statistics
  }

  public get isVisiting() {
    return this.type === HomeConfTypes.visiting
  }
}
