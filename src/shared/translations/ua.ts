const base = {
  smartJournal: 'Smart Journal',
  digitalJournal: 'цифровий журнал в твоїй кишені',
  login: 'Вхід',
  signin: 'Реєстрація',
  email: 'Емейл',
  password: 'Пароль',
  notEmpty: 'Поле не повинне бути пустим',
  min: 'символів мінімум',
  max: 'символів максимум',
  emailExample: 'Приклад: netotemail@gmail.com',
  changePassword: 'Змінити пароль',
}

export const startPageText = {
  title: base.smartJournal,
  login: base.login,
  signin: base.signin,
  slides: {
    addClub: 'Реєструй клуб',
    addGroup: 'Створюй групи',
    addStudent: 'Довавай людей',
    addVisiting: 'Відмічай відвідування тренування',
    addMoney: 'Слідкуй за грошима',
    viewStatistics: 'Переглядай статистику',
    relax: 'Розслабся',
  },
}

export const signUpText = {
  title: base.signin,
  smartJournal: base.smartJournal,
  digitalJournal: base.digitalJournal,
  email: base.email,
  password: base.password,
  notEmpty: base.notEmpty,
  min: base.min,
  max: base.max,
  emailExample: base.emailExample,
  signup: base.signin,
  agreements: [
    'Натискаючи на цю кнопку, Ви даєте',
    'згоду на обробку персональних даних',
    'та',
    'погоджуєтеся c політикою конфіденційності',
  ],
}

export const tncText = {
  title: 'Персональні дані',
  personalData: {
    title: 'Згода на обробку персональних данних',
    content: `Реєструючись в даному додатку Ви даєте згоду на обробку своїх персональних данних.
    Це може бути ім'я, прізвище, електронна пошта, номер телефону та не тільки. Ці данні не будуть
    передаватися третім лицям та будуть використанні лише для зручності користування додатком`,
  },
  privacyPolicy: {
    title: 'Політика конфіденційності',
    content: [
      `Реєструючись в даному додатку Ви погоджуєтесь з політикою конфіденційності.
      ${base.smartJournal} серйозно ставиться до питання приватності. Нижче викладені п’ять принципів,
      що лежать в основі нашого підходу до поваги конфіденційності:`,
      `Ми цінуємо те, що Ви довіряєте нам свою особисту інформацію.
      Ми завжди використовуватимемо вашу інформацію лише чесним та гідним вашої довіри шляхом.`,
      `Ви маєте право на чітку інформацію про те, як ми використовуємо вашу особисту
      інформацію. Ми завжди будемо прозорими з вами щодо того, яку інформацію ми збираємо, що ми з нею робимо,
      з ким нею ділимося та до кого вам слід звернутися, якщо у вас виникають сумніви.`,
      `Якщо у вас є які-небудь питання про те, як ми використовуємо вашу особисту інформацію,
      ми будемо працювати з вами, щоб оперативно відповісти на ці запитання.`,
      `Ми будемо тримати вашу інформацію в безпеці, та приймати всіх розумних заходів,
      щоб вберегти її від неправомірного використання.`,
      `Ми будемо дотримуватися всіх законів, що захищають приватну інформацію, а також співпрацюватимемо з органами
      влади. У разі відсутності відповідного законодавства, ми діятимемо у відповідності із загальнопринятими
      принципами щодо захисту конфіденційної інформації.`,
    ],
  },
}

export const loginText = {
  title: base.login,
  smartJournal: base.smartJournal,
  digitalJournal: base.digitalJournal,
  email: base.email,
  password: base.password,
  notEmpty: base.notEmpty,
  min: base.min,
  max: base.max,
  emailExample: base.emailExample,
  login: base.login,
  forgotPassword: 'Забув пароль?',
}

export const forgotPasswordText = {
  title: base.changePassword,
  email: base.email,
  notEmpty: base.notEmpty,
  min: base.min,
  max: base.max,
  emailExample: base.emailExample,
  changePassword: base.changePassword,
  note: `Введіть Вашу електронну адресу та натисніть на кнопку,
  після Вам на пошту прийде лист з посиланням для зміни паролю`,
}

export const signupSuccessText = {
  title: 'Успіх',
  signupSuccess: 'Вітаю, реєстрація пройшла успішно',
  note: `Перевірте пошту, Вам був надісланий лист для підтвердження, перейдіть
  по посиланню та повертайтеся в додаток для авторизації та подальших дій`,
  action: 'Я підтвердив пошту, продовжити',
}

export const authFormText = {
  blocked: 'Користувач заблокований адміністратором, зверніться в підтримку',
  alreadyInUse: 'Вказаний email вже використовується іншим аккаунтом',
  minLengthInvalid: 'Пароль повинний бути не менше 6ти символів',
  emailInvalid: 'Вказаний email має невірний формат',
  passwordIncorrect: 'Невірний пароль',
  notFound: 'Користувача з даним email не знайдено',
  confirmEmail: 'Підтвердіть вказаний email, лист з посиланням вже надісланий',
}
