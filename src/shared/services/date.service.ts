import { DatePipe } from '@angular/common'
import { Injectable } from '@angular/core'

interface IDayItem {
  name: string,
  value: number,
}

@Injectable()
export class DateService {
  public monthesMap = {
    'Jan': 'Січ',
    'Feb': 'Лют',
    'Mar': 'Бер',
    'Apr': 'Кві',
    'May': 'Тра',
    'Jun': 'Чер',
    'Jul': 'Лип',
    'Aug': 'Сер',
    'Sep': 'Вер',
    'Oct': 'Жов',
    'Nov': 'Лис',
    'Dec': 'Гру',
  }

  public monthsFullMap = {
    'January': 'Січень',
    'February': 'Лютий',
    'March': 'Березень',
    'April': 'Квітень',
    'May': 'Травень',
    'June': 'Червень',
    'July': 'Липень',
    'August': 'Серпень',
    'September': 'Вересень',
    'October': 'Жовтень',
    'November': 'Листопад',
    'December': 'Грудень',
  }

  public daysOfWeekMap = {
    'Mon': 'Пн',
    'Tue': 'Вт',
    'Wed': 'Ср',
    'Thu': 'Чт',
    'Fri': 'Пт',
    'Sat': 'Сб',
    'Sun': 'Нд',
  }

  public dayNumbersOfWeekMap = [
    'Нд',
    'Пн',
    'Вт',
    'Ср',
    'Чт',
    'Пт',
    'Сб',
  ]

  public montheNumberMap = {
    0: 'Січень',
    1: 'Лютий',
    2: 'Березень',
    3: 'Квітень',
    4: 'Травень',
    5: 'Червень',
    6: 'Липень',
    7: 'Серпень',
    8: 'Вересень',
    9: 'Жовтень',
    10: 'Листопад',
    11: 'Грудень',
  }

  public monthNumberMap = {
    0: 'Січ',
    1: 'Лют',
    2: 'Бер',
    3: 'Кві',
    4: 'Тра',
    5: 'Чер',
    6: 'Лип',
    7: 'Сер',
    8: 'Вер',
    9: 'Жов',
    10: 'Лис',
    11: 'Гру',
  }

  public daysList: IDayItem[] = [
    {
      name: 'Понеділок',
      value: 1,
    },
    {
      name: 'Вівторок',
      value: 2,
    },
    {
      name: 'Середа',
      value: 3,
    },
    {
      name: 'Четвер',
      value: 4,
    },
    {
      name: "П'ятниця",
      value: 5,
    },
    {
      name: 'Субота',
      value: 6,
    },
    {
      name: 'Неділя',
      value: 0,
    },
  ]

  public daysMap: string[] = [
    'Неділя',
    'Понеділок',
    'Вівторок',
    'Середа',
    'Четвер',
    "П'ятниця",
    'Субота',
  ]

  private _datePipe: DatePipe
  constructor() {
      this._datePipe = new DatePipe('en-US')
  }

  public formatDate(createDate: number, format?: string) {
      !format ? format = 'EEE dd MMM' : ''

      const date = this._datePipe.transform(new Date(createDate), format)
      const today = this._datePipe.transform(new Date(), format)
      const yesterday = this._datePipe.transform(new Date().setDate(new Date().getDate()  - 1), format)

      if (date === today) {
        return 'Сьогодні'
      } else if (date === yesterday) {
        return 'Вчора'
      } else {
        return this.translateDate(date, format)
      }
  }

  public getTrainingDayDate(trainingDate: string) { // 2018/8/27
      let y = parseInt(trainingDate.split('/')[0], 10)
      let m = parseInt(trainingDate.split('/')[1], 10)
      let d = parseInt(trainingDate.split('/')[2], 10)
      let format = 'EEE dd MMMM y'
      return this.formatDate(new Date(y, m, d).getTime(), format)
  }

  public translateDate(date: string, format: string) {
    if (date && date.length) {
      const spliter = ' '
      const d = date.split(spliter)
      if (format === 'EEE dd MMM') {
        return this.daysOfWeekMap[d[0]] + spliter
                + d[1] + spliter +
                this.monthesMap[d[2]]
      } else if (format === 'EEE dd MMMM y') {
        return this.daysOfWeekMap[d[0]] + spliter
                + d[1] + spliter
                + this.monthsFullMap[d[2]] + spliter
                + d[3]
      }
    }
  }
  public parseYearWithMonth(dateForAccess: string) { // '2018/1'
      if (dateForAccess) {
          return this.montheNumberMap[parseInt(dateForAccess.split('/')[1], 10)] + ' ' + dateForAccess.split('/')[0]
      }
  }
  public parseYearWithMonthsArray(array: string[]) {
    let str = ''
    if (!array) { return str }

    array.forEach((s: string) => {
        str = str + this.parseYearWithMonth(s) + ' '
    })
    return str
  }
  public parseMonth(m: string) { // '1' February
      if (m) {
          return this.montheNumberMap[parseInt(m, 10)]
      }
  }
}
