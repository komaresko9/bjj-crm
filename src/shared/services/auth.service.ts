import { Injectable } from '@angular/core'
import { Storage } from '@ionic/storage'
import * as firebase from 'firebase'

import { NgRedux } from '@angular-redux/store'
import { AuthActions } from '../store/actions/auth'
import { IState } from '../store/store'

@Injectable()
export class AuthService {

  constructor(
    private storage: Storage,
    private store: NgRedux<IState>,
    private actions: AuthActions,
  ) {
  }

  public signIn(email: string, password: string): Promise<void> {

    this.store.dispatch(this.actions.signInRequest())

    const onError = (error) => {
      this.store.dispatch(this.actions.signInFailure(error))
    }

    const onSuccess = (user) => {
      this.store.dispatch(this.actions.signInSuccess(user))
    }

    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
        .then((auth: firebase.auth.UserCredential) => {
          const user = auth.user
          if (user.emailVerified) {
            onSuccess(user)
          } else {
            let error: any = { code: 'auth/email-not-verified', message: 'Email is not verified.' }
            user
              .sendEmailVerification()
                .then(() => onError(error))
                .catch(() => onError(error))
          }
        })
        .catch(onError)
  }

  public signUp(email: string, password: string): Promise<void> {

    this.store.dispatch(this.actions.signUpRequest())

    const onError = (error) => {
      this.store.dispatch(this.actions.signUpFailure(error))
    }

    const onSuccess = () => {
      this.signOut().then(() => {
        this.store.dispatch(this.actions.signUpSuccess())
      })
    }

    return firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((auth: firebase.auth.UserCredential) => {
        return auth.user
          .sendEmailVerification()
            .then(onSuccess, onError)
            .catch(onError)
      }, onError)
      .catch(onError)
  }

  public forgotPassword(email: string): Promise<void> {

    this.store.dispatch(this.actions.forgotPasswordRequest())

    const onError = (error) => {
      this.store.dispatch(this.actions.forgotPasswordFailure(error))
    }
    const onSuccess = () => {
      this.store.dispatch(this.actions.forgotPasswordSuccess())
    }

    return firebase.auth().sendPasswordResetEmail(email)
      .then(onSuccess)
      .catch(onError)
  }

  public signOut(): Promise<void> {

    this.store.dispatch(this.actions.signOutRequest())

    const onError = (error) => {
      this.store.dispatch(this.actions.signOutFailure(error))
    }
    const onSuccess = () => {
      this.store.dispatch(this.actions.signOutSuccess())
      this.storage.remove('state')
      this.storage.clear()

    }

    return firebase.auth().signOut()
      .then(onSuccess)
      .catch(onError)

  }
}
