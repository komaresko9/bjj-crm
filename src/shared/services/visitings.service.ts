import { Injectable } from '@angular/core'

import { Visiting } from '../models/visiting.model'
import { FireProvider } from '../providers/fire.provider'
import { RefsProvider } from '../providers/refs.provider'
import { ClubsService } from '../services/clubs.service'

import { NgRedux } from '@angular-redux/store'
import { VisitingActions } from '../store/actions/visiting'
import { IState } from '../store/store'

@Injectable()
export class VisitingsService {

  public visitings: Visiting[] = []

  constructor(
      public clubsService: ClubsService,
      private store: NgRedux<IState>,
      private actions: VisitingActions,
    ) {
  }

  // public getByDate(firstDay: Date, lastDay: Date): Promise<Visiting[]> {
  public get(dates: Date[] = []): Promise<void> {
    const firstDay = dates[0].getTime()
    const lastDay = dates[dates.length - 1].getTime()
    this.store.dispatch(this.actions.getReset())
    this.store.dispatch(this.actions.getRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.getFailure(error))
    }

    const onSuccess = (resp: firebase.firestore.QuerySnapshot) => {
      if (resp.empty) {
        throw {
          code: 'visitings/empty',
          message: `There is no visitings for the requested club and date range`,
        } as firebase.FirebaseError
      }
      const visitings = this.getRespToVisitingsMap(resp)
      this.store.dispatch(this.actions.getSuccess(visitings))
    }

    try {
      return FireProvider.store
        .collection(RefsProvider.visitings)
        .where('club', '==', this.store.getState().club.data.key)
        .where('date', '>=', firstDay)
        .where('date', '<=', lastDay)
        .get()
        .then(onSuccess, onError)
        .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public getRespToVisitingsMap(resp: firebase.firestore.QuerySnapshot) {
    let visitings = {
      map: [],
      loadingMap: {},
    }
    resp
      .forEach((doc: firebase.firestore.QueryDocumentSnapshot) => {
        const visiting = doc.data() as Visiting
        visitings = this.visitingToVisitingsMap(visitings, visiting)
      })
    return visitings
  }

  public visitingToVisitingsMap(visitings: any, visiting: Visiting) {
    const groupKey = visiting.group
    const clientKey = visiting.client
    const dateKey = visiting.date + ''

    if (!visitings.map[groupKey]) {
      visitings.map[groupKey] = []
      visitings.map.length += 1
    }

    if (!visitings.map[groupKey][clientKey]) {
      visitings.map[groupKey][clientKey] = []
      visitings.map[groupKey].length += 1
    }
    visitings.map[groupKey][clientKey][dateKey] = visiting
    visitings.map[groupKey][clientKey].length += 1

    if (!visitings.loadingMap[groupKey]) {
      visitings.loadingMap[groupKey] = {}
    }
    if (!visitings.loadingMap[groupKey][clientKey]) {
      visitings.loadingMap[groupKey][clientKey] = {}
    }
    visitings.loadingMap[groupKey][clientKey][dateKey] = undefined

    return visitings
  }

  public create(visiting: any) {
    this.store.dispatch(this.actions.createRequest(visiting))

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.createFailure(visiting, error))
    }

    const onSuccess = (vis: Visiting) => {
      let visitings = {
        map: [],
        loadingMap: {},
      }
      this.visitingToVisitingsMap(visitings, vis)
      this.store.dispatch(this.actions.createSuccess(visitings))
    }

    try {
      return FireProvider.store
      .collection(RefsProvider.visitings)
      .add(visiting)
      .then((data: firebase.firestore.DocumentReference) => {
        visiting.key = data.id
        return FireProvider.store
          .collection(RefsProvider.visitings)
          .doc(visiting.key)
          .update(visiting)
            .then(() => { onSuccess(visiting) }, onError)
            .catch(onError)
      })
      .catch(onError)
    } catch (error) {
      onError(error)
    }
  }
  public update(visiting: any) {

    this.store.dispatch(this.actions.updateRequest(visiting))

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.updateFailure(visiting, error))
    }
    const onSuccess = () => { this.store.dispatch(this.actions.updateSuccess(visiting)) }

    try {
      return FireProvider.store
        .collection(RefsProvider.visitings)
        .doc(visiting.key)
        .update(visiting)
          .then(onSuccess, onError)
          .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public delete(visiting: any) {
    this.store.dispatch(this.actions.deleteRequest(visiting))

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.deleteFailure(visiting, error))
    }

    const onSuccess = () => {
      this.store.dispatch(this.actions.deleteSuccess(visiting))
    }
    try {
      return FireProvider.store
        .collection(RefsProvider.visitings)
        .doc(visiting.key)
        .delete()
        .then(onSuccess, onError)
        .catch(onError)
    } catch (error) {
      onError(error)
    }
  }
}
