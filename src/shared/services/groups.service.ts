import { Injectable, OnDestroy } from '@angular/core'

import { NgRedux } from '@angular-redux/store'
import { Group } from '../models/group.model'
import { ISchedule } from '../models/schedule.model'
import { FireProvider } from '../providers/fire.provider'
import { RefsProvider } from '../providers/refs.provider'
import { GroupActions } from '../store/actions/group'
import { IState } from '../store/store'

@Injectable()
export class GroupsService implements OnDestroy {

  public selectedGroupKey: string
  public groups: Group[] = []
  public groupsMap: Group[] = []

  public loaded = false

  constructor(
    private store: NgRedux<IState>,
    private actions: GroupActions,
  ) {
  }

  public get (): Promise<void | firebase.FirebaseError> {

    this.store.dispatch(this.actions.getReset())
    this.store.dispatch(this.actions.getRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.getFailure(error))
    }

    const onSuccess = (resp: firebase.firestore.QuerySnapshot) => {
      if (resp.empty) {
        throw {
          code: 'groups/empty',
          message: `There is no groups for the requested club`,
        } as firebase.FirebaseError
      }
      const groupsData = this.getRespToGroupsData(resp)
      this.store.dispatch(this.actions.getSuccess(groupsData))
    }

    try {
      return FireProvider.store
        .collection(RefsProvider.groups)
        .where('club', '==', this.store.getState().club.data.key)
        .get()
        .then(onSuccess, onError)
        .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public getRespToGroupsData(resp: firebase.firestore.QuerySnapshot) {
    const groupsData = {
      selectedGroupKey: '',
      list: [],
      map: {},
    }
    resp.forEach((doc: firebase.firestore.QueryDocumentSnapshot) => {
      let group = doc.data() as Group
      if (!groupsData.selectedGroupKey) {
        groupsData.selectedGroupKey = group.key
      }
      groupsData.map[group.key] = group
      groupsData.list.push(group)
    })
    return groupsData
  }

  public create(newGroup: any): Promise<void> {
    this.store.dispatch(this.actions.getReset())
    this.store.dispatch(this.actions.createReset())
    this.store.dispatch(this.actions.createRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.createFailure(error))
    }

    const onSuccess = (group: Group) => {
      this.store.dispatch(this.actions.createSuccess(group))
    }

    try {
      return FireProvider.store
        .collection(RefsProvider.groups)
        .add(newGroup)
        .then((data: firebase.firestore.DocumentReference ) => {
          newGroup.key = data.id
          return FireProvider.store
            .collection(RefsProvider.groups)
            .doc(newGroup.key)
            .update(newGroup)
            .then(() => { onSuccess(newGroup) }, onError)
            .catch(onError)
        }, onError)
      .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public getName(key: string): string {
    try {
      return this.groupsMap[key].name
    } catch (error) {
      return
    }
  }

  public getSelectedGroup(): Group {
    return this.groupsMap && this.selectedGroupKey
      ? this.groupsMap[this.selectedGroupKey]
      : undefined
  }

  public ngOnDestroy() {
    this.groups = []
    delete this.groupsMap
    this.loaded = false
  }
}
