import { NgRedux } from '@angular-redux/store'
import { Injectable } from '@angular/core'

import { Channel } from '../models/channel.model'
import { FireProvider } from '../providers/fire.provider'
import { RefsProvider } from '../providers/refs.provider'
import { ChannelActions } from '../store/actions/channel'
import { IState } from '../store/store'

@Injectable()
export class ChannelsService {

  constructor(
    private actions: ChannelActions,
    private store: NgRedux<IState>,
  ) {
    // let channelNames = ['Знайомі', 'Facebook', 'Instagram']
    // channelNames.forEach((name: string) => {
    //   let c = new Channel(name)
    //   this.create(Object.assign({}, c))
    // })
    // this.get()
  }

  public get(): Promise<void | firebase.FirebaseError> {
    this.store.dispatch(this.actions.getReset())
    this.store.dispatch(this.actions.getRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.getFailure(error))
    }

    const onSuccess = (resp: firebase.firestore.QuerySnapshot) => {
      if (resp.empty) {
        throw {
          code: 'channels/empty',
          message: `There is no channels for the requested environment`,
        } as firebase.FirebaseError
      }
      const channelsList = this.getRespToChannelsData(resp)
      this.store.dispatch(this.actions.getSuccess(channelsList))
    }

    try {
      return FireProvider.store
        .collection(RefsProvider.channels)
        .get()
        .then(onSuccess, onError)
        .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public getRespToChannelsData(resp: firebase.firestore.QuerySnapshot) {
    const list = []
    resp.forEach((doc: firebase.firestore.QueryDocumentSnapshot) => {
      let channel = doc.data() as Channel
      list.push(channel)
    })
    return list
  }

  // public create(newChannel: any) {
  //   if (newChannel) {
  //     return FireProvider.store.collection(RefsProvider.channels)
  //     .add(newChannel)
  //     .then((data: firebase.firestore.DocumentReference) => {
  //       newChannel.key = data.id
  //       FireProvider.store
  //         .collection(RefsProvider.channels)
  //         .doc(newChannel.key)
  //         .update(newChannel)
  //         .catch((error) => { return error })
  //     })
  //     .catch((error) => { return error })
  //   }
  // }
}
