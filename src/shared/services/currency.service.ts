import { Injectable } from '@angular/core'

@Injectable()
export class CurrencyService {

    private selectedCurrency: string = '₴'

    public getSelectedCurrency() {
        return this.selectedCurrency
    }
}
