import { Injectable } from '@angular/core'

import { NgRedux } from '@angular-redux/store'
import { Club } from '../models/club.model'
import { FireProvider } from '../providers/fire.provider'
import { RefsProvider } from '../providers/refs.provider'
import { ClubActions } from '../store/actions/club'
import { IState } from '../store/store'

@Injectable()
export class ClubsService {

  constructor(
    private store: NgRedux<IState>,
    private actions: ClubActions,
  ) {}

  public get(): Promise<void | firebase.FirebaseError> {
    this.store.dispatch(this.actions.createReset())
    this.store.dispatch(this.actions.getRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.getFailure(error))
    }

    const onSuccess = (resp: firebase.firestore.QuerySnapshot) => {
      if (resp.empty) {
        throw {
          code: 'club/empty',
          message: `There is no club for the requested user`,
        } as firebase.FirebaseError
      }
      const club = this.getRespToClub(resp)
      this.store.dispatch(this.actions.getSuccess(club))
    }

    try {
      return FireProvider.store
      .collection(RefsProvider.clubs)
      .where('user', '==', this.store.getState().auth.user.uid)
      .get()
      .then(onSuccess, onError)
      .catch(onError)
    } catch (error) {
      onError(error)
      throw error
    }
  }

  public create(newClub: any): Promise<void | firebase.FirebaseError> {
    this.store.dispatch(this.actions.getReset())
    this.store.dispatch(this.actions.createReset())
    this.store.dispatch(this.actions.createRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.createFailure(error))
    }

    const onSuccess = (club: Club) => {
      this.store.dispatch(this.actions.createSuccess(club))
    }
    try {
      return FireProvider.store
      .collection(RefsProvider.clubs)
      .add(newClub)
      .then((data: firebase.firestore.DocumentReference ) => {
        newClub.key = data.id
        return FireProvider.store
          .collection(RefsProvider.clubs)
          .doc(newClub.key)
          .update(newClub)
          .then(() => onSuccess(newClub))
          .catch(onError)
      })
      .catch(onError)
    } catch (error) {
      onError(error)
      throw error
    }
  }

  private getRespToClub(resp: firebase.firestore.QuerySnapshot): Club {
    let club: Club
    resp.forEach((doc: firebase.firestore.QueryDocumentSnapshot) => {
      club = doc.data() as Club
    })
    return club
  }
}
