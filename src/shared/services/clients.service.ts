import { NgRedux } from '@angular-redux/store'
import { Injectable } from '@angular/core'
import * as _ from 'lodash'

import { FireProvider } from '../providers/fire.provider'
import { GroupsService } from '../services/groups.service'

import { Client } from '../models/client.model'
import { RefsProvider } from '../providers/refs.provider'
import { ClientActions } from '../store/actions/client'
import { IState } from '../store/store'

@Injectable()
export class ClientsService {

  public clients: Client[] = []
  public clientsMap: Client[] = []
  public clientsGroupMap: Client[] = []
  public loaded = false

  constructor(
    public groupsService: GroupsService,
    private store: NgRedux<IState>,
    private actions: ClientActions,
  ) {
  }

  public get(): Promise<void | firebase.FirebaseError> {
    this.store.dispatch(this.actions.getReset())
    this.store.dispatch(this.actions.getRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.getFailure(error))
    }

    const onSuccess = (resp: firebase.firestore.QuerySnapshot) => {
      if (resp.empty) {
        throw {
          code: 'clients/empty',
          message: `There is no clients for the requested club`,
        } as firebase.FirebaseError
      }
      const clientsData = this.getRespToClientsData(resp)
      this.store.dispatch(this.actions.getSuccess(clientsData))
    }

    try {
      return FireProvider.store
      .collection(RefsProvider.clients)
      .where('club', '==', this.store.getState().club.data.key)
      .get()
      .then(onSuccess, onError)
      .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public getRespToClientsData(resp: firebase.firestore.QuerySnapshot) {
    const clientsData = {
      list: [],
      mapByGroupKey: {},
    }
    resp.forEach((doc: firebase.firestore.QueryDocumentSnapshot) => {
      let client = doc.data() as Client
      clientsData.list.push(client)

      client.group.forEach((groupKey: string) => {
        if (!clientsData.mapByGroupKey[groupKey]) {
          clientsData.mapByGroupKey[groupKey] = []
        }
        const indexOfClientInMapByGroupKey =
          clientsData.mapByGroupKey[groupKey]
            .findIndex((c: Client) => c.key === client.key)

        if (indexOfClientInMapByGroupKey > -1) {
          clientsData.mapByGroupKey[groupKey].splice(indexOfClientInMapByGroupKey, 1)
        }
        clientsData.mapByGroupKey[groupKey].push(client)
      })
    })
    return clientsData
  }

  public create(newClient: any) {

    this.store.dispatch(this.actions.createReset())
    this.store.dispatch(this.actions.createRequest())

    const onError = (error: firebase.FirebaseError) => {
      this.store.dispatch(this.actions.createFailure(error))
    }

    const onSuccess = (client: Client) => {
      const clientsData = this.getClientToClientsData(client)
      this.store.dispatch(this.actions.createSuccess(clientsData))
    }

    try {
      return FireProvider.store
      .collection(RefsProvider.clients)
      .add(newClient)
      .then((data: firebase.firestore.DocumentReference) => {
        newClient.key = data.id
        return FireProvider.store
        .collection(RefsProvider.clients)
        .doc(newClient.key)
        .update(newClient)
          .then(() => { onSuccess(newClient) })
          .catch(onError)
      })
      .catch(onError)
    } catch (error) {
      onError(error)
    }
  }

  public getClientToClientsData(client: Client) {
    const clientsData = {
      list: [],
      mapByGroupKey: {},
    }
    clientsData.list.push(client)
    client.group.forEach((groupKey: string) => {
      clientsData.mapByGroupKey[groupKey] = []
      clientsData.mapByGroupKey[groupKey].push(client)
    })
    return clientsData
  }

  public get clientList(): Client[] {
    return this.clientsGroupMap[this.store.getState().groups.data.selectedGroupKey]
  }

  public getClientList(groupKey: string): Client[] {
    return groupKey && this.clientsGroupMap[groupKey]
      ? this.clientsGroupMap[groupKey]
      : []
  }
}
