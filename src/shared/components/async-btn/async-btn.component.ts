import { Component, Input } from '@angular/core'
import { Observable } from 'rxjs'

export interface IAsyncBtnData {
  text: string,
  isLoading: Observable<boolean>,
  isDisabled: boolean,
  action?: Function,
}

@Component({
  selector: 'async-btn',
  templateUrl: 'async-btn.component.html',
})

export class AsyncBtn {
  private d: IAsyncBtnData
  private isLoading: boolean
  @Input() public set data(data: IAsyncBtnData) {
    this.d = data
    this.d.isLoading.subscribe((value) => {
      this.isLoading = value
    })
  }

  public get isDisabled(): boolean {
    return this.d.isDisabled
  }

  public get text(): string {
    return this.d.text
  }

  public action() {
    this.d.action()
  }
}
