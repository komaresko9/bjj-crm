import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges,
} from '@angular/core'
import { DateService } from '../../services/date.service'

@Component({
    selector: 'ion-calendar-month-selector',
    templateUrl: './calendar-month-selector.html',
})

export class CalendarMonthSelector implements OnChanges {

    @Output() public onBack = new EventEmitter<void>()
    @Output() public onForward = new EventEmitter<void>()

    @Input() public year: number
    @Input() public month: number

    public get monthLabel() {
      return this.dateService.monthNumberMap[this.month]
    }
    public get yearLabel() {
      try {
        let y = this.year.toString()
        y = y[y.length - 2] + y[y.length - 1]
        return y
      } catch (error) {
       return
      }
    }

    constructor(
        private dateService: DateService,
    ) {}

    public ngOnChanges(changes: SimpleChanges) {
      if (changes.year && this.year !== changes.year.currentValue) {
        this.year = changes.year.currentValue
      }
      if (changes.month && this.month !== changes.month.currentValue) {
        this.month = changes.month.currentValue
      }
    }
}