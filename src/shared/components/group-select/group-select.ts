import { Component } from '@angular/core'
import { Group } from '../../models/group.model'

import { NgRedux, select } from '@angular-redux/store'
import { Observable } from 'rxjs'
import { GroupActions } from '../../store/actions/group'
import { IState } from '../../store/store'

@Component({
  selector: 'group-select',
  templateUrl: 'group-select.html',
})
export class GroupSelectComponent {
  public text = {
    emptyGroup: 'Додайте першу групу',
    selectedGroup: 'Вибрана група в журналі',
  }

  @select(['groups', 'data', 'selectedGroupKey']) public selectedGroupKey$: Observable<string>
  @select(['groups', 'data', 'list']) public list$: Observable<any>

  constructor(
    private store: NgRedux<IState>,
    private actions: GroupActions,
  ) { }

  public select(group: Group): void {
    this.store.dispatch(this.actions.select(group.key))
  }
}
