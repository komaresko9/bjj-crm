import { Component, Input } from '@angular/core'
import { Group } from '../../models/group.model'
import { ClientsService } from '../../services/clients.service'

import * as _ from 'lodash'

@Component({
  selector: 'groups-list',
  templateUrl: 'groups-list.component.html',
  styles: ['groups-list.component.scss'],
})

export class GroupsList {

  public text = {
    emptyList: 'Додайте свою першу групу',
    emptySearch: 'Не знайдено',
    clients: 'студентів',
    amountDay: '',
    amountMonth: '',
    edit: 'Редагувати',
    hide: 'Прибрати',
    fromList: 'зі списку',
  }

  public list: Group[] = []
  @Input() public data: Group[]
  public get filter(): string {
    return this.f
  }
  @Input() public set filter(str: string) {
    this.f = str
    this.reloadList()
  }
  private f: string = ''

  constructor(
    public clientsService: ClientsService,
  ) { }

  public getClientsLength(key: string): number {
    return this.clientsService.getClientList(key).length
  }

  private reloadList(): void {
    this.list = _.cloneDeep(this.data)
    if (this.filter && this.filter.trim() !== '') {
      this.list = this.list.filter((item: Group) => {
        return ((item.name + item.amountDay + item.amountMonth).toLowerCase().indexOf(this.filter.toLowerCase()) > -1)
      })
    }
  }
}
