import { NgModule } from '@angular/core'
import { IonicModule } from 'ionic-angular'
import { AsyncBtn } from './async-btn/async-btn.component'
import { CalendarMonthSelector } from './calendar-month-selector/calendar-month-selector'
import { ClientsList } from './clients-list/clients-list.component'
import { Counter } from './counter/counter.component'
import { GroupSelectComponent } from './group-select/group-select'
import { GroupsList } from './groups-list/groups-list.component'
import { Visiting } from './visiting/visiting.component'

@NgModule({
  declarations: [
    AsyncBtn,
    CalendarMonthSelector,
    GroupsList,
    ClientsList,
    GroupSelectComponent,
    Visiting,
    Counter,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    AsyncBtn,
    CalendarMonthSelector,
    GroupsList,
    ClientsList,
    GroupSelectComponent,
    Visiting,
    Counter,
  ],
  entryComponents: [
    AsyncBtn,
    CalendarMonthSelector,
    GroupsList,
    ClientsList,
    GroupSelectComponent,
    Visiting,
    Counter,
  ],
})
export class ComponentsModule {}
