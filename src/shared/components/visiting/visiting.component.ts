import { Component, Input } from '@angular/core'
import { AlertController } from 'ionic-angular'
import { Client } from '../../models/client.model'
import { VisitingsService } from '../../services/visitings.service'

@Component({
  selector: 'visiting',
  templateUrl: 'visiting.component.html',
  styles: ['visiting.component.scss'],
})

export class Visiting {

  public text = {
    title: 'Скільки грошей здав',
    placeholder: 'Введіть суму',
    save: 'Зберегти',
    cancel: 'Cancel',
  }

  @Input() public visitingsLoadingMap: any
  @Input() public visitingsMap: any
  @Input() public isMoney: boolean
  @Input() public client: Client
  @Input() public clubKey: string
  @Input() public groupKey: string
  private pDate: any | Date
  public get date() {
    return this.pDate.getTime()
  }
  @Input() public set date(date) {
    this.pDate = date
  }
  constructor(
    private alert: AlertController,
    private visitingsService: VisitingsService,
  ) {  }

  public get key(): string {
    try {
      return this.visitingsMap[this.groupKey][this.client.key][this.date].key
    } catch (error) {
      return ''
    }
  }

  public get showCheckmark(): boolean {
    try {
      return this.visitingsMap[this.groupKey][this.client.key][this.date].key && !this.showCheckmarkSpinner
    } catch (error) {
      return
    }
  }

  public get showNoData(): boolean {
    return
    // return !this.total[this.client.key]
  }

  public get showCheckmarkSpinner(): boolean {
    try {
      return this.visitingsLoadingMap[this.groupKey][this.client.key][this.date]
    } catch (error) {
      return
    }
  }

  public get showCircle(): boolean {
    return !this.showCheckmark && !this.showCheckmarkSpinner
  }

  public get amount() {
    try {
      return this.visitingsMap[this.groupKey][this.client.key][this.date].amount
    } catch (error) {
      return 0
    }
  }

  public get isDebt() {
    try {
      const a = this.visitingsMap[this.groupKey][this.client.key][this.date].amount
      return !a || a === 0
    } catch (error) {
      return
    }
  }

  public checkoutVisiting(amout?: number) {
    let visiting = {
      client: this.client.key,
      date: this.date,
      group: this.groupKey,
      club: this.clubKey,
      key: this.key,
      amount: amout || 0,
    }

    try {
      if (this.showCheckmark && !amout) {
        this.visitingsService.delete(visiting)
      } else if (this.showCheckmark && amout) {
        this.visitingsService.update(visiting)
      } else {
        this.visitingsService.create(visiting)
      }
    } catch (error) {
      console.warn(error)
    }
  }

  public checkoutMoney() {
    const confirm = this.alert.create({
      title: `${this.text.title} ${this.client.firstName} ${this.client.lastName}
      ${this.pDate}`,
      // message: 'Деньги',
      inputs: [
        {
          name: 'amount',
          placeholder: this.text.placeholder,
          type: 'number',
          value: this.amount || '',
        },
      ],
      buttons: [
        { text: this.text.cancel },
        {
          text: this.text.save,
          handler: (data: any) => {
            this.checkoutVisiting(parseFloat(data.amount))
          },
        },
      ],
    })
    confirm.present()
  }
}
