import { Component, Input } from '@angular/core'
import { Client } from '../../models/client.model'
import { Group } from '../../models/group.model'
import { CurrencyService } from '../../services/currency.service'

@Component({
  selector: 'counter',
  templateUrl: 'counter.component.html',
  styles: ['counter.component.scss'],
})

export class Counter {
  @Input() public visitingsMap: any
  @Input() public isMoney: boolean
  @Input() public isDebt: boolean
  @Input() public isDebtCounter: boolean
  @Input() public group: Group
  @Input() public client: Client
  @Input() public groupKey: string
  private pDates: Date[] = []
  @Input() public set dates(dates: Date[]) {
    this.pDates = dates
  }

  constructor(
    public currencyService: CurrencyService,
  ) {
  }

  public get selectedMonth() {
    return this.pDates[0].getMonth()
  }

  public get visitingsCounter() {
    try {
      let counter = 0
      Object.keys(this.visitingsMap[this.groupKey][this.client.key])
        .forEach((d: string) => {
          if (new Date(parseInt(d, 10)).getMonth() === this.selectedMonth) {
            counter ++
          }
        })
      return counter
    } catch (error) {
      return 0
    }
  }

  public get amountCounter() {
    try {
      let amount = 0
      Object.keys(this.visitingsMap[this.groupKey][this.client.key])
        .forEach((d: string) => {
          const day = parseInt(d, 10)
          if (new Date(day).getMonth() === this.selectedMonth) {
            amount = amount + this.visitingsMap[this.groupKey][this.client.key][day].amount
          }
        })
      return /*this.currencyService.getSelectedCurrency() + */amount
    } catch (error) {
      return 0
    }
  }

  public get amountDebt() {
    try {
      const v = this.visitingsCounter
      const a = this.amountCounter
      let d = v * parseInt(this.group.amountDay.toString(), 10)
      if (d > parseInt(this.group.amountMonth.toString(), 10)) {
        d = parseInt(this.group.amountMonth.toString(), 10)
      }
      d -= a
      return d
    } catch (error) {
      return 0
    }
  }

  public get debtCounter() {
    return 2
  }
}
