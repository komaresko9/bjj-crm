import { Component, Input } from '@angular/core'
import { Client } from '../../models/client.model'
import { GroupsService } from '../../services/groups.service'

import * as _ from 'lodash'

@Component({
  selector: 'clients-list',
  templateUrl: 'clients-list.component.html',
  styles: ['clients-list.component.scss'],
})

export class ClientsList {

  public text = {
    emptyList: 'Додайте першого студента в групу ' + this.getGroupName,
    emptySearch: 'Не знайдено',
    edit: 'Редагувати',
    hide: 'Прибрати',
    fromList: 'зі списку',
  }

  public get data(): Client[] {
    return this.d
  }
  @Input() public set data(data: Client[]) {
    this.d = data || []
    this.reloadList()
  }
  public list: Client[] = []
  public get filter(): string {
    return this.f
  }
  @Input() public set filter(str: string) {
    this.f = str || ''
    this.reloadList()
  }
  private f: string = ''
  private d: Client[] = []

  constructor(
    public groupsService: GroupsService,
  ) { }

  public callTo(telephoneNumber: string): void {
    window.open(`tel:${telephoneNumber}`, '_system')
  }

  private get getGroupName(): string {
    return this.groupsService.loaded && this.groupsService.selectedGroupKey
      ? this.groupsService.getSelectedGroup().name
      : ''
  }

  private reloadList() {
    this.list = _.cloneDeep(this.data)
    if (this.filter && this.filter.trim() !== '') {
      this.list = this.list.filter((item: Client) => {
        return ((item.firstName + item.lastName + item.telephone).toLowerCase().indexOf(this.filter.toLowerCase()) > -1)
      })
    }
  }
}
