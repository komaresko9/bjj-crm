import {Pipe} from '@angular/core'
import * as _ from 'lodash'

@Pipe({ name: 'order' })
export class OrderByPipe {
  public transform(array, args) {
    _.sortBy(array, args)
    return array.reverse()
  }
}
