import {Pipe} from '@angular/core'

@Pipe({ name: 'months' })
export class SortMonthsPipe {
  public transform(array: string[]) {
    // array: ['1','5','3','8']
    for (let i = 0; i < array.length; i++) {
      for (let j = 0; j < array.length; j++) {
        const item_j = array[j]
        const item_i = array[i]
        // item_i = '0'
        // item_j = '1'
        // if years equal
        // comparing by the month
        if (parseInt(item_i, 10) > parseInt(item_j, 10)) {
          const a = array[i]
          array[i] = array[j]
          array[j] = a
        }
      }
    }
    return array // array: ['8','5','3','1']
  }
}
