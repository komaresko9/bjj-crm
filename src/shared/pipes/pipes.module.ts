import { NgModule } from '@angular/core'

import { OrderByPipe } from './order-by.pipe'
import { SortMonthsPipe } from './sort-months.pipe'

@NgModule({
  declarations: [
    OrderByPipe,
    SortMonthsPipe,
  ],
})
export class PipesModule {}
