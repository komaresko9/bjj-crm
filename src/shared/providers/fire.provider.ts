import { Injectable } from '@angular/core'
import * as firebase from 'firebase'

@Injectable()
export class FireProvider {

  public static get store () {
    let store = firebase.firestore()
    store.settings({})
    return store
  }
}
