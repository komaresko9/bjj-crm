import { Injectable } from '@angular/core'

enum Refs {
  accounts = 'accounts',
  channels = 'channels',
  clients = 'clients',
  groups = 'groups',
  money = 'money',
  trainings = 'trainings',
  clubs = 'clubs',
  visitings = 'visitings',
}

@Injectable()
export class RefsProvider {

  public static get account(): Refs {
    return Refs.accounts
  }
  public static get channels(): Refs {
    return Refs.channels
  }
  public static get clients(): Refs {
    return Refs.clients
  }
  public static get groups(): Refs {
    return Refs.groups
  }
  public static get money(): Refs {
    return Refs.money
  }
  public static get trainings(): Refs {
    return Refs.trainings
  }
  public static get clubs(): Refs {
    return Refs.clubs
  }
  public static get visitings(): Refs {
    return Refs.visitings
  }
}
