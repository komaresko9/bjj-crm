import { Injectable } from '@angular/core'

@Injectable()
export class PatternsProvider {
  public static get min() {
    return 6
  }
  public static get max() {
    return 30
  }
  public static get currencyPattern() {
    return /(\d+\.\d{1,2})/
  }
  public static get hrefPattern() {
    return /(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/
  }
  public static get quatityPattern() {
    return /^[1-9][0-9]*$/
  }
  public static get emailPattern() {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  }
  public static get phonePattern() {
    return /^[+]{0,1}[-\s\./0-9]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/
  }
  public static get phonePrefixPattern() {
    return /^\+[0-9]*$/
  }
  public static get numberPattern() {
    return /^[0-9]*$/
  }
  public static get namePattert() {
    return /^[А-яа-яъёіїa-zАа-яЪЁІЇАA-Z][а-яъёіїa-zА-ЯЪЁІЇA-Z .,'-]*$/
  }
  public static get isoPattern() {
    return /^(AF|AX|AL|DZ|AS|AD|AO|AI|AQ|AG|AR|AM|AW|AU|AT|AZ|BS|BH|BD|BB|BY|BE|BZ|BJ|BM|BT|BO|BQ|BA|BW|BV|BR|IO|BN|BG|BF|BI|KH|CM|CA|CV|KY|CF|TD|CL|CN|CX|CC|CO|KM|CG|CD|CK|CR|CI|HR|CU|CW|CY|CZ|DK|DJ|DM|DO|EC|EG|SV|GQ|ER|EE|ET|FK|FO|FJ|FI|FR|GF|PF|TF|GA|GM|GE|DE|GH|GI|GR|GL|GD|GP|GU|GT|GG|GN|GW|GY|HT|HM|VA|HN|HK|HU|IS|IN|ID|IR|IQ|IE|IM|IL|IT|JM|JP|JE|JO|KZ|KE|KI|KP|KR|KW|KG|LA|LV|LB|LS|LR|LY|LI|LT|LU|MO|MK|MG|MW|MY|MV|ML|MT|MH|MQ|MR|MU|YT|MX|FM|MD|MC|MN|ME|MS|MA|MZ|MM|NA|NR|NP|NL|NC|NZ|NI|NE|NG|NU|NF|MP|NO|OM|PK|PW|PS|PA|PG|PY|PE|PH|PN|PL|PT|PR|QA|RE|RO|RU|RW|BL|SH|KN|LC|MF|PM|VC|WS|SM|ST|SA|SN|RS|SC|SL|SG|SX|SK|SI|SB|SO|ZA|GS|SS|ES|LK|SD|SR|SJ|SZ|SE|CH|SY|TW|TJ|TZ|TH|TL|TG|TK|TO|TT|TN|TR|TM|TC|TV|UG|UA|AE|GB|US|UM|UY|UZ|VU|VE|VN|VG|VI|WF|EH|YE|ZM|ZW)$/
  }
  public static get zipMask() {
    return [/[a-zA-Z]/, /[a-zA-Z]/, '-', /[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/]
  }
  public static get personalNumberMask() {
    return /^\d{2}([0][1-9]|[1][0-2])([0][1-9]|[1-2][0-9]|[3][0-1])-\d{4}$/
  }
  public static get phoneMask() {
    return ['+', /[1-9]/, /[0-9]/, ' ', '(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/]
  }
  public static get companyIdMask() {
    return [/[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  }
}
