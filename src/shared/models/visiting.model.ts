
export class Visiting {
    constructor(public client: string,
                public date: any,
                public club: string,
                public group: string,
                public key: string,
                public amout?: number,
                ) {}
}
