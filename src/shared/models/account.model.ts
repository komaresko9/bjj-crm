
export class Account {
    constructor(public createDate: number,
                public amount: number,
                public name: string,
                public peopleHowCanSee: string[],
                public key: string,
            ) {
    }
}
