export class Club {
    public isDeleted: boolean
    constructor(
                public name: string,
                public user: string,
                public key?: string) {
                    this.isDeleted = false
                }
}
