
export class Client {
    public firstName: string
    public lastName: string
    public club: string
    public group: string[]
    public key: string
    public channel: string
    public isDeleted: boolean
    public createDate: number
    public telephone: string

    constructor(data: any) {
        this.firstName = data.firstName || ''
        this.lastName = data.lastName || ''
        this.club = data.club || ''
        this.group = data.group || []
        this.telephone = data.telephone || ''
        this.key = data.key || ''
        this.channel = data.channel || ''
        this.createDate = new Date().getTime()
        this.isDeleted = false
    }
}
