export interface ISchedule {
  day: number,
  hh: string,
  mm: string,
}
