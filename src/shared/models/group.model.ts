import { ISchedule } from './schedule.model'

export class Group {
  public isDeleted: boolean
  constructor(
    public name: string,
    public amountMonth: number,
    public amountDay: number,
    public club: string,
    public schedule: ISchedule[],
    public key: string) {
      this.isDeleted = false
    }
}
