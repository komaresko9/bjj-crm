
export class Channel {
    public name: string
    public key: string
    public isDeleted: boolean
    constructor(name) {
        this.name = name
        this.key = ''
        this.isDeleted = false
    }
}
