import { Action } from 'redux'
import { IState } from '../store'
import { AUTH_ACTION_TYPES } from '../types'

export const authReducer = (state: IState, action: Action | any) => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    // SIGN_IN
    case AUTH_ACTION_TYPES.SIGN_IN_REQUEST:
      newState.auth.info.signIn = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    case AUTH_ACTION_TYPES.SIGN_IN_SUCCESS:
      newState.auth.info.signIn.isLoading = false
      newState.auth.info.signIn.isSuccess = true
      newState.auth.info.signOut = {} as any
      newState.auth.user = action.user
      newState.auth.isLoggedIn = true
      return newState
    case AUTH_ACTION_TYPES.SIGN_IN_FAILURE:
      newState.auth.info.signIn = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    // SIGN_UP
    case AUTH_ACTION_TYPES.SIGN_UP_REQUEST:
      newState.auth.info.signUp = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    case AUTH_ACTION_TYPES.SIGN_UP_SUCCESS:
      newState.auth.info.signUp.isLoading = false
      newState.auth.info.signUp.isSuccess = true
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    case AUTH_ACTION_TYPES.SIGN_UP_FAILURE:
      newState.auth.info.signUp = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
      case AUTH_ACTION_TYPES.SIGN_UP_RESET:
      newState.auth.info.signUp = {
        isLoading: false,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    // FORGOT_PASSWORD
    case AUTH_ACTION_TYPES.FORGOT_PASSWORD_REQUEST:
      newState.auth.info.forgotPassword = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    case AUTH_ACTION_TYPES.FORGOT_PASSWORD_SUCCESS:
      newState.auth.info.forgotPassword.isLoading = false
      newState.auth.info.forgotPassword.isSuccess = true
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    case AUTH_ACTION_TYPES.FORGOT_PASSWORD_FAILURE:
      newState.auth.info.forgotPassword = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    case AUTH_ACTION_TYPES.FORGOT_PASSWORD_RESET:
      newState.auth.info.forgotPassword = {
        isLoading: false,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    // SIGN_OUT
    case AUTH_ACTION_TYPES.SIGN_OUT_REQUEST:
      newState.auth.info.signOut = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      return newState
    case AUTH_ACTION_TYPES.SIGN_OUT_SUCCESS:
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      newState.auth.info.signOut.isLoading = false
      newState.auth.info.signOut.isSuccess = true
      newState.auth.info.signIn = {} as any
      newState.auth.info.signUp = {} as any
      newState.auth.info.forgotPassword = {} as any

      newState.club.data = undefined
      newState.club.info.get = {} as any
      newState.club.info.create = {} as any

      newState.groups.data = {
        selectedGroupKey: undefined,
        list: [],
        map: {},
      }
      newState.groups.info.get = {} as any
      newState.groups.info.create = {} as any

      return newState
    case AUTH_ACTION_TYPES.SIGN_OUT_FAILURE:
      newState.auth.info.signOut = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.auth.user = undefined
      newState.auth.isLoggedIn = false
      return newState
    default: {
      return state
    }
  }
}
