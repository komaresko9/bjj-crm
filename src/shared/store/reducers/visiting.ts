import * as deepmerge from 'deepmerge'
import { Action } from 'redux'
import { IState, IStateDataInfo } from '../store'
import { VISITING_ACTION_TYPES } from '../types'

export const visitingReducer = (state: IState, action: Action | any) => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    // GET
    case VISITING_ACTION_TYPES.VISITING_GET_REQUEST:
      newState.visitings.info.get = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      return newState
    case VISITING_ACTION_TYPES.VISITING_GET_SUCCESS:
      newState.visitings.info.get.isLoading = false
      newState.visitings.info.get.isSuccess = true

      for (const groupKey in action.visitings.map) {
        if (action.visitings.map.hasOwnProperty(groupKey)) {
          if (!newState.visitings.data.map[groupKey]) {
            newState.visitings.data.map[groupKey] = {}
          }
          for (const clientKey in action.visitings.map[groupKey]) {
            if (action.visitings.map[groupKey].hasOwnProperty(clientKey)) {
              if (!newState.visitings.data.map[groupKey][clientKey]) {
                newState.visitings.data.map[groupKey][clientKey] = {}
              }
              newState.visitings.data.map[groupKey][clientKey] = Object.assign({
                ...newState.visitings.data.map[groupKey][clientKey],
                ...action.visitings.map[groupKey][clientKey],
              }, {})
            }
          }
        }
      }

      for (const groupKey in  action.visitings.loadingMap) {
        if ( action.visitings.loadingMap.hasOwnProperty(groupKey)) {
          if (!newState.visitings.info.loadingMap[groupKey]) {
            newState.visitings.info.loadingMap[groupKey] = {}
          }
          for (const clientKey in  action.visitings.loadingMap[groupKey]) {
            if ( action.visitings.loadingMap[groupKey].hasOwnProperty(clientKey)) {
              if (!newState.visitings.info.loadingMap[groupKey][clientKey]) {
                newState.visitings.info.loadingMap[groupKey][clientKey] = {}
              }
              newState.visitings.info.loadingMap[groupKey][clientKey] = Object.assign({
                ...newState.visitings.info.loadingMap[groupKey][clientKey],
                ... action.visitings.loadingMap[groupKey][clientKey],
              }, {})
            }
          }
        }
      }

      return newState
    case VISITING_ACTION_TYPES.VISITING_GET_FAILURE:
      newState.visitings.info.get = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      return newState
    case VISITING_ACTION_TYPES.VISITING_GET_RESET:
      newState.visitings.info.get = {} as IStateDataInfo
      return newState
    // CREAT
    case VISITING_ACTION_TYPES.VISITING_CREATE_REQUEST:
      const gKey = action.visiting.group
      const cKey = action.visiting.client
      const dKey = action.visiting.date
      if (!newState.visitings.info.loadingMap[gKey]) {
        newState.visitings.info.loadingMap[gKey] = {}
      }
      if (!newState.visitings.info.loadingMap[gKey][cKey]) {
        newState.visitings.info.loadingMap[gKey][cKey] = {}
      }
      if (!newState.visitings.info.loadingMap[gKey][cKey][dKey]) {
        newState.visitings.info.loadingMap[gKey][cKey][dKey] = {}
      }
      newState.visitings.info.loadingMap[gKey][cKey][dKey] = true
      return newState
    case VISITING_ACTION_TYPES.VISITING_CREATE_SUCCESS:
      for (const groupKey in action.visitings.map) {
        if (action.visitings.map.hasOwnProperty(groupKey)) {
          if (!newState.visitings.data.map[groupKey]) {
            newState.visitings.data.map[groupKey] = {}
          }
          for (const clientKey in action.visitings.map[groupKey]) {
            if (action.visitings.map[groupKey].hasOwnProperty(clientKey)) {
              if (!newState.visitings.data.map[groupKey][clientKey]) {
                newState.visitings.data.map[groupKey][clientKey] = {}
              }
              newState.visitings.data.map[groupKey][clientKey] = Object.assign({
                ...newState.visitings.data.map[groupKey][clientKey],
                ...action.visitings.map[groupKey][clientKey],
              }, {})
            }
          }
        }
      }

      for (const groupKey in  action.visitings.loadingMap) {
        if ( action.visitings.loadingMap.hasOwnProperty(groupKey)) {
          if (!newState.visitings.info.loadingMap[groupKey]) {
            newState.visitings.info.loadingMap[groupKey] = {}
          }
          for (const clientKey in  action.visitings.loadingMap[groupKey]) {
            if ( action.visitings.loadingMap[groupKey].hasOwnProperty(clientKey)) {
              if (!newState.visitings.info.loadingMap[groupKey][clientKey]) {
                newState.visitings.info.loadingMap[groupKey][clientKey] = {}
              }
              newState.visitings.info.loadingMap[groupKey][clientKey] = Object.assign({
                ...newState.visitings.info.loadingMap[groupKey][clientKey],
                ... action.visitings.loadingMap[groupKey][clientKey],
              }, {})
            }
          }
        }
      }

      return newState
    case VISITING_ACTION_TYPES.VISITING_CREATE_FAILURE:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client] = {}
      return newState
    case VISITING_ACTION_TYPES.VISITING_CREATE_RESET:
      newState.visitings.info.create = {} as IStateDataInfo
      return newState
    // DELETE
    case VISITING_ACTION_TYPES.VISITING_DELETE_REQUEST:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client][action.visiting.date] = true
      return newState
    case VISITING_ACTION_TYPES.VISITING_DELETE_SUCCESS:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client] = {}
      delete newState.visitings.data.map[action.visiting.group][action.visiting.client][action.visiting.date]
      return newState
    case VISITING_ACTION_TYPES.VISITING_DELETE_FAILURE:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client] = {}
      return newState
    case VISITING_ACTION_TYPES.VISITING_DELETE_RESET:
      newState.visitings.info.delete = {} as IStateDataInfo
      return newState
    // UPDATE
    case VISITING_ACTION_TYPES.VISITING_UPDATE_REQUEST:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client][action.visiting.date] = true
      return newState
    case VISITING_ACTION_TYPES.VISITING_UPDATE_SUCCESS:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client] = {}
      newState.visitings.data.map[action.visiting.group][action.visiting.client][action.visiting.date] = action.visiting
      return newState
    case VISITING_ACTION_TYPES.VISITING_UPDATE_FAILURE:
      newState.visitings.info.loadingMap[action.visiting.group][action.visiting.client] = {}
      return newState
    case VISITING_ACTION_TYPES.VISITING_UPDATE_RESET:
      newState.visitings.info.update = {} as IStateDataInfo
      return newState
    default: {
      return state
    }
  }
}
