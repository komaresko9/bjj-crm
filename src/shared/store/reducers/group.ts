import { Action } from 'redux'
import { IState, IStateDataInfo } from '../store'
import { GROUP_ACTION_TYPES } from '../types'

import { merge } from 'lodash'

export const groupReducer = (state: IState, action: Action | any) => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    // SELECTED
    case GROUP_ACTION_TYPES.GROUP_SELECTED:
      newState.groups.data.selectedGroupKey = action.selectedGroupKey
      return newState
    // GET
    case GROUP_ACTION_TYPES.GROUP_GET_REQUEST:
      newState.groups.info.get = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.groups.data = {
        selectedGroupKey: undefined,
        list: undefined,
        map: undefined,
      }
      return newState
    case GROUP_ACTION_TYPES.GROUP_GET_SUCCESS:
      newState.groups.info.get.isLoading = false
      newState.groups.info.get.isSuccess = true
      newState.groups.data.selectedGroupKey = action.groupsData.selectedGroupKey
      newState.groups.data.list = merge(action.groupsData.list, newState.groups.data.list)
      newState.groups.data.map = merge(action.groupsData.map, newState.groups.data.map)
      return newState
    case GROUP_ACTION_TYPES.GROUP_GET_FAILURE:
      newState.groups.info.get = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.groups.data.selectedGroupKey = undefined
      newState.groups.data.list = []
      newState.groups.data.map = {}

      return newState
    case GROUP_ACTION_TYPES.GROUP_GET_RESET:
      newState.groups.info.get = {} as IStateDataInfo
      newState.groups.data.selectedGroupKey = undefined
      return newState
    // CREAT
    case GROUP_ACTION_TYPES.GROUP_CREATE_REQUEST:
      newState.groups.info.create = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      return newState
    case GROUP_ACTION_TYPES.GROUP_CREATE_SUCCESS:
      newState.groups.info.create.isLoading = false
      newState.groups.info.create.isSuccess = true
      newState.groups.data.selectedGroupKey = action.group.key
      newState.groups.data.list.push(action.group)
      newState.groups.data.map[action.group.key] = action.group
      return newState
    case GROUP_ACTION_TYPES.GROUP_CREATE_FAILURE:
      newState.groups.info.create = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      return newState
    case GROUP_ACTION_TYPES.GROUP_CREATE_RESET:
      newState.groups.info.create = {} as IStateDataInfo
      newState.groups.data.selectedGroupKey = undefined
      return newState
    default: {
      return state
    }
  }
}
