import { Action } from 'redux'
import { IState, IStateDataInfo } from '../store'
import { CHANNEL_ACTION_TYPES } from '../types'

export const channelReducer = (state: IState, action: Action | any) => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    // GET
    case CHANNEL_ACTION_TYPES.CHANNEL_GET_REQUEST:
      newState.channels.info.get = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.channels.data.list = undefined
      return newState
    case CHANNEL_ACTION_TYPES.CHANNEL_GET_SUCCESS:
      newState.channels.info.get.isLoading = false
      newState.channels.info.get.isSuccess = true
      newState.channels.data.list = action.channelsList
      return newState
    case CHANNEL_ACTION_TYPES.CHANNEL_GET_FAILURE:
      newState.channels.info.get = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.channels.data.list = []

      return newState
    case CHANNEL_ACTION_TYPES.CHANNEL_GET_RESET:
      newState.channels.info.get = {} as IStateDataInfo
      return newState
    default: {
      return state
    }
  }
}
