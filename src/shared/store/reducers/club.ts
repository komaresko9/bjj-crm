import { Action } from 'redux'
import { IState, IStateDataInfo } from '../store'
import { CLUB_ACTION_TYPES } from '../types'

export const clubReducer = (state: IState, action: Action | any) => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    // GET
    case CLUB_ACTION_TYPES.CLUB_GET_REQUEST:
      newState.club.info.get = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.club.data = undefined
      return newState
    case CLUB_ACTION_TYPES.CLUB_GET_SUCCESS:
      newState.club.info.get.isLoading = false
      newState.club.info.get.isSuccess = true
      newState.club.data = action.club
      return newState
    case CLUB_ACTION_TYPES.CLUB_GET_FAILURE:
      newState.club.info.get = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.club.data = undefined
      return newState
    case CLUB_ACTION_TYPES.CLUB_GET_RESET:
      newState.club.info.get = {} as IStateDataInfo
      newState.club.data = undefined
      return newState
    // CREATE
    case CLUB_ACTION_TYPES.CLUB_CREATE_REQUEST:
      newState.club.info.create = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.club.data = undefined
      return newState
    case CLUB_ACTION_TYPES.CLUB_CREATE_SUCCESS:
      newState.club.info.create.isLoading = false
      newState.club.info.create.isSuccess = true
      newState.club.data = action.club
      return newState
    case CLUB_ACTION_TYPES.CLUB_CREATE_FAILURE:
      newState.club.info.create = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.club.data = undefined
      return newState
    case CLUB_ACTION_TYPES.CLUB_CREATE_RESET:
      newState.club.info.create = {} as IStateDataInfo
      newState.club.data = undefined
      return newState
    default: {
      return state
    }
  }
}
