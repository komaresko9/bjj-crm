import * as deepmerge from 'deepmerge'
import { Action } from 'redux'
import { IState, IStateDataInfo } from '../store'
import { CLIENT_ACTION_TYPES } from '../types'

export const clientReducer = (state: IState, action: Action | any) => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    // GET
    case CLIENT_ACTION_TYPES.CLIENT_GET_REQUEST:
      newState.clients.info.get = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      newState.clients.data = {
        list: undefined,
        mapByGroupKey: undefined,
      }
      return newState
    case CLIENT_ACTION_TYPES.CLIENT_GET_SUCCESS:
      newState.clients.info.get.isLoading = false
      newState.clients.info.get.isSuccess = true
      newState.clients.data.list = action.clientsData.list
      newState.clients.data.mapByGroupKey = action.clientsData.mapByGroupKey
      return newState
    case CLIENT_ACTION_TYPES.CLIENT_GET_FAILURE:
      newState.clients.info.get = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      newState.clients.data.list = []
      newState.clients.data.mapByGroupKey = {}

      return newState
    case CLIENT_ACTION_TYPES.CLIENT_GET_RESET:
      newState.clients.info.get = {} as IStateDataInfo
      return newState
    // CREAT
    case CLIENT_ACTION_TYPES.CLIENT_CREATE_REQUEST:
      newState.clients.info.create = {
        isLoading: true,
        isSuccess: false,
        isError: false,
        error: undefined,
      }
      return newState
    case CLIENT_ACTION_TYPES.CLIENT_CREATE_SUCCESS:
      newState.clients.info.create.isLoading = false
      newState.clients.info.create.isSuccess = true
      newState.clients.data = deepmerge(newState.clients.data, action.clientsData)
      return newState
    case CLIENT_ACTION_TYPES.CLIENT_CREATE_FAILURE:
      newState.clients.info.create = {
        isLoading: false,
        isSuccess: false,
        isError: true,
        error: action.error,
      }
      return newState
    case CLIENT_ACTION_TYPES.CLIENT_CREATE_RESET:
      newState.clients.info.create = {} as IStateDataInfo
      return newState
    default: {
      return state
    }
  }
}
