import { Storage, StorageConfig } from '@ionic/storage'
import { Action } from 'redux'

import {
  AUTH_ACTION_TYPES,
  CHANNEL_ACTION_TYPES,
  CLIENT_ACTION_TYPES,
  CLUB_ACTION_TYPES,
  GROUP_ACTION_TYPES,
  VISITING_ACTION_TYPES,
} from './types'

import { authReducer } from './reducers/auth'
import { channelReducer } from './reducers/channel'
import { clientReducer } from './reducers/clients'
import { clubReducer } from './reducers/club'
import { groupReducer } from './reducers/group'
import { visitingReducer } from './reducers/visiting'

import { Channel } from '../models/channel.model'
import { Client } from '../models/client.model'
import { Club } from '../models/club.model'
import { Group } from '../models/group.model'
import { Visiting } from '../models/visiting.model'

export interface IStateDataInfo {
  isSuccess: boolean,
  isLoading: boolean,
  isError: boolean,
  error: any,
}

export interface IAuthStateData {
  user: firebase.User,
  isLoggedIn: boolean,
  info: {
    signIn: IStateDataInfo,
    signUp: IStateDataInfo,
    forgotPassword: IStateDataInfo,
    signOut: IStateDataInfo,
  }
}

export interface IClubStateData {
  info: {
    get: IStateDataInfo,
    create: IStateDataInfo,
  }
  data: Club,
}

export interface IGroupsStateData {
  info: {
    get: IStateDataInfo,
    create: IStateDataInfo,
  }
  data: {
    selectedGroupKey: string,
    list: Group[],
    map: {} ,
  },
}

export interface IClientsStateData {
  info: {
    get: IStateDataInfo,
    create: IStateDataInfo,
  }
  data: {
    list: Client[],
    mapByGroupKey: {},
  },
}

export interface IChannelsStateData {
  info: {
    get: IStateDataInfo,
  }
  data: {
    list: Channel[],
  },
}

// export interface ILoadingMapItem<T> {
  // [key: string]: {
    // [key: string]: T,
  // },
// }

export interface IVisitingsStateData {
  info: {
    get: IStateDataInfo,
    create: IStateDataInfo,
    delete: IStateDataInfo,
    update: IStateDataInfo,
    loadingMap: {},
  }
  data: {
    map: {},
  },
}

export interface IState {
  auth: IAuthStateData,
  club: IClubStateData,
  groups: IGroupsStateData,
  clients: IClientsStateData,
  channels: IChannelsStateData,
  visitings: IVisitingsStateData,
}

const storage = new Storage('localstorage' as StorageConfig)

export const INITIAL_STATE: IState = {
  auth: {
    user: undefined,
    isLoggedIn: false,
    info: {
      signIn: {},
      signUp: {},
      forgotPassword: {},
      signOut: {},
    },
  } as IAuthStateData,
  club: {
    info: {
      get: {},
      create: {},
    },
    data: undefined,
  } as IClubStateData,
  groups: {
    info: {
      get: {},
      create: {},
    },
    data: {
      selectedGroupKey: undefined,
      list: [],
      map: {},
    },
  } as IGroupsStateData,
  clients: {
    info: {
      get: {},
      create: {},
    },
    data: {
      list: [],
      mapByGroupKey: {},
    },
  } as IClientsStateData,
  channels: {
    info: {
      get: {},
    },
    data: {
      list: [],
    },
  } as IChannelsStateData,
  visitings: {
    info: {
      get: {},
      create: {},
      delete: {},
      update: {},
      loadingMap: {},
    },
    data: {
      map: {},
    },
  } as IVisitingsStateData,
}

// use storage from cache
export const GET_INITIAL_STATE = () => {
  return storage.get('state')
    .then((state) => {
      return state || INITIAL_STATE
    }, () => { return INITIAL_STATE })
    .catch(() => { return INITIAL_STATE })
}

export function rootReducer(state: IState, action: Action): IState {
  let newState = getState(state, action)
  storage.set('state', newState)
  console.warn(action.type, newState)
  return newState
}

function getState(state: IState, action: Action): IState {
  if (AUTH_ACTION_TYPES[action.type]) {
    return authReducer(state, action)
  } else if (CLUB_ACTION_TYPES[action.type]) {
    return clubReducer(state, action)
  } else if (GROUP_ACTION_TYPES[action.type]) {
    return groupReducer(state, action)
  } else if (CLIENT_ACTION_TYPES[action.type]) {
    return clientReducer(state, action)
  } else if (CHANNEL_ACTION_TYPES[action.type]) {
    return channelReducer(state, action)
  } else if (VISITING_ACTION_TYPES[action.type]) {
    return visitingReducer(state, action)
  } else {
    return state
  }
}
