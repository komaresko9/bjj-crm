import { NgModule } from '@angular/core'
import { IonicModule } from 'ionic-angular'

import { AuthActions } from './auth'
import { ChannelActions } from './channel'
import { ClientActions } from './client'
import { ClubActions } from './club'
import { GroupActions } from './group'
import { VisitingActions } from './visiting'

@NgModule({
  imports: [
    IonicModule,
  ],
  providers: [
    AuthActions,
    ClubActions,
    GroupActions,
    ClientActions,
    ChannelActions,
    VisitingActions,
  ],
})
export class ActionsModule {}
