import { Injectable } from '@angular/core'
import { Action } from 'redux'
import { Client } from '../../models/client.model'
import { CLIENT_ACTION_TYPES } from '../types'

@Injectable()
export class ClientActions {

  public getRequest(): Action {
    return { type: CLIENT_ACTION_TYPES.CLIENT_GET_REQUEST } as Action
  }
  public getSuccess(clientsData): Action {
    return {
      type: CLIENT_ACTION_TYPES.CLIENT_GET_SUCCESS,
      clientsData,
    } as Action
  }
  public getFailure(error): Action {
    return {
      type: CLIENT_ACTION_TYPES.CLIENT_GET_FAILURE,
      error,
    } as Action
  }
  public getReset(): Action {
    return { type: CLIENT_ACTION_TYPES.CLIENT_GET_RESET } as Action
  }
  public createRequest(): Action {
    return { type: CLIENT_ACTION_TYPES.CLIENT_CREATE_REQUEST } as Action
  }
  public createSuccess(clientsData): Action {
    return {
      type: CLIENT_ACTION_TYPES.CLIENT_CREATE_SUCCESS,
      clientsData,
    } as Action
  }
  public createFailure(error): Action {
    return {
      type: CLIENT_ACTION_TYPES.CLIENT_CREATE_FAILURE,
      error,
    } as Action
  }
  public createReset(): Action {
    return { type: CLIENT_ACTION_TYPES.CLIENT_CREATE_RESET } as Action
  }
}
