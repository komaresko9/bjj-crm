import { Injectable } from '@angular/core'
import { Action } from 'redux'
import { Group } from '../../models/group.model'
import { GROUP_ACTION_TYPES } from '../types'

@Injectable()
export class GroupActions {

  public select(selectedGroupKey: string): Action {
    return {
      selectedGroupKey,
      type: GROUP_ACTION_TYPES.GROUP_SELECTED,
    } as Action
  }

  public getRequest(): Action {
    return { type: GROUP_ACTION_TYPES.GROUP_GET_REQUEST } as Action
  }
  public getSuccess(groupsData): Action {
    return {
      type: GROUP_ACTION_TYPES.GROUP_GET_SUCCESS,
      groupsData,
    } as Action
  }
  public getFailure(error): Action {
    return {
      type: GROUP_ACTION_TYPES.GROUP_GET_FAILURE,
      error,
    } as Action
  }
  public getReset(): Action {
    return { type: GROUP_ACTION_TYPES.GROUP_GET_RESET } as Action
  }
  public createRequest(): Action {
    return { type: GROUP_ACTION_TYPES.GROUP_CREATE_REQUEST } as Action
  }
  public createSuccess(group: Group): Action {
    return {
      type: GROUP_ACTION_TYPES.GROUP_CREATE_SUCCESS,
      group,
    } as Action
  }
  public createFailure(error): Action {
    return {
      type: GROUP_ACTION_TYPES.GROUP_CREATE_FAILURE,
      error,
    } as Action
  }
  public createReset(): Action {
    return { type: GROUP_ACTION_TYPES.GROUP_CREATE_RESET } as Action
  }
}
