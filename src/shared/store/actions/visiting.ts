import { Injectable } from '@angular/core'
import { Action } from 'redux'
import { VISITING_ACTION_TYPES } from '../types'

@Injectable()
export class VisitingActions {

  public getRequest(): Action {
    return { type: VISITING_ACTION_TYPES.VISITING_GET_REQUEST } as Action
  }
  public getSuccess(visitings): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_GET_SUCCESS,
      visitings,
    } as Action
  }
  public getFailure(error): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_GET_FAILURE,
      error,
    } as Action
  }
  public getReset(): Action {
    return { type: VISITING_ACTION_TYPES.VISITING_GET_RESET } as Action
  }
  public createRequest(visiting): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_CREATE_REQUEST,
      visiting,
    } as Action
  }
  public createSuccess(visitings): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_CREATE_SUCCESS,
      visitings,
    } as Action
  }
  public createFailure(visiting, error): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_CREATE_FAILURE,
      visiting,
      error,
    } as Action
  }
  public createReset(): Action {
    return { type: VISITING_ACTION_TYPES.VISITING_CREATE_RESET } as Action
  }
  public deleteRequest(visiting): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_DELETE_REQUEST,
      visiting,
    } as Action
  }
  public deleteSuccess(visiting): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_DELETE_SUCCESS,
      visiting,
    } as Action
  }
  public deleteFailure(visiting, error): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_DELETE_FAILURE,
      visiting,
      error,
    } as Action
  }
  public deleteReset(): Action {
    return { type: VISITING_ACTION_TYPES.VISITING_DELETE_RESET } as Action
  }
  public updateRequest(visiting): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_UPDATE_REQUEST,
      visiting,
    } as Action
  }
  public updateSuccess(visiting): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_UPDATE_SUCCESS,
      visiting,
    } as Action
  }
  public updateFailure(visiting, error): Action {
    return {
      type: VISITING_ACTION_TYPES.VISITING_UPDATE_FAILURE,
      visiting,
      error,
    } as Action
  }
  public updateReset(): Action {
    return { type: VISITING_ACTION_TYPES.VISITING_UPDATE_RESET } as Action
  }
}
