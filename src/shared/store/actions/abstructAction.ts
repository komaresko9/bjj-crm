import { Injectable } from '@angular/core'
import { Action } from 'redux'

@Injectable()
export class AbstructAction {

  constructor(
      private requested: string,
      private loaded: string,
      private added: string,
      private updated: string,
      private deleted: string,
      private error: string,
    ) {

  }

  public requestSuccess(filter): Action {
    return {
      type: this.requested,
      payload: filter,
    } as Action
  }
  public loadSuccess(data): Action {
    return {
      type: this.loaded,
      payload: data,
    } as Action
  }
  public addSuccess(data): Action {
    return {
      type: this.added,
      payload: data,
    } as Action
  }
  public updateSuccess(data): Action {
    return {
      type: this.updated,
      payload: data,
    } as Action
  }
  public deleteSuccess(data): Action {
    return {
      type: this.deleted,
      payload: data,
    } as Action
  }

  public getError(data): Action {
    return {
      type: this.error,
      payload: data,
    } as Action
  }
}
