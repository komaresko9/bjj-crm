import { Injectable } from '@angular/core'
import { Action } from 'redux'
import { Channel } from '../../models/channel.model'
import { CHANNEL_ACTION_TYPES } from '../types'

@Injectable()
export class ChannelActions {

  public getRequest(): Action {
    return { type: CHANNEL_ACTION_TYPES.CHANNEL_GET_REQUEST } as Action
  }
  public getSuccess(channelsList: Channel[]): Action {
    return {
      type: CHANNEL_ACTION_TYPES.CHANNEL_GET_SUCCESS,
      channelsList,
    } as Action
  }
  public getFailure(error): Action {
    return {
      type: CHANNEL_ACTION_TYPES.CHANNEL_GET_FAILURE,
      error,
    } as Action
  }
  public getReset(): Action {
    return { type: CHANNEL_ACTION_TYPES.CHANNEL_GET_RESET } as Action
  }
}
