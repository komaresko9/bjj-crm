import { Injectable } from '@angular/core'
import { User } from 'firebase'
import { Action } from 'redux'
import { AUTH_ACTION_TYPES } from '../types'

@Injectable()
export class AuthActions {
  public signInRequest(): Action {
    return { type: AUTH_ACTION_TYPES.SIGN_IN_REQUEST } as Action
  }
  public signInSuccess(user: User): Action {
    return {
      type: AUTH_ACTION_TYPES.SIGN_IN_SUCCESS,
      user,
      isLoggedIn: true,
    } as Action
  }
  public signInFailure(error): Action {
    return {
      type: AUTH_ACTION_TYPES.SIGN_IN_FAILURE,
      error,
    } as Action
  }
  public signUpRequest(): Action {
    return { type: AUTH_ACTION_TYPES.SIGN_UP_REQUEST } as Action
  }
  public signUpSuccess(): Action {
    return { type: AUTH_ACTION_TYPES.SIGN_UP_SUCCESS } as Action
  }
  public signUpFailure(error): Action {
    return {
      type: AUTH_ACTION_TYPES.SIGN_UP_FAILURE,
      error,
    } as Action
  }
  public signUpReset(): Action {
    return { type: AUTH_ACTION_TYPES.SIGN_UP_RESET } as Action
  }
  public forgotPasswordRequest(): Action {
    return { type: AUTH_ACTION_TYPES.FORGOT_PASSWORD_REQUEST } as Action
  }
  public forgotPasswordSuccess(): Action {
    return { type: AUTH_ACTION_TYPES.FORGOT_PASSWORD_SUCCESS } as Action
  }
  public forgotPasswordFailure(error): Action {
    return {
      type: AUTH_ACTION_TYPES.FORGOT_PASSWORD_FAILURE,
      error,
    } as Action
  }
  public forgotPasswordReset(): Action {
    return { type: AUTH_ACTION_TYPES.FORGOT_PASSWORD_RESET } as Action
  }
  public signOutRequest(): Action {
    return { type: AUTH_ACTION_TYPES.SIGN_OUT_REQUEST } as Action
  }
  public signOutSuccess(): Action {
    return { type: AUTH_ACTION_TYPES.SIGN_OUT_SUCCESS } as Action
  }
  public signOutFailure(error): Action {
    return {
      type: AUTH_ACTION_TYPES.SIGN_OUT_FAILURE,
      error,
    } as Action
  }
}
