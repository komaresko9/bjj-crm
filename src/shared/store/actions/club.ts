import { Injectable } from '@angular/core'
import { Action } from 'redux'
import { Club } from '../../models/club.model'
import { CLUB_ACTION_TYPES } from '../types'

@Injectable()
export class ClubActions {

  public getRequest(): Action {
    return { type: CLUB_ACTION_TYPES.CLUB_GET_REQUEST } as Action
  }
  public getSuccess(club: Club): Action {
    return {
      type: CLUB_ACTION_TYPES.CLUB_GET_SUCCESS,
      club,
    } as Action
  }
  public getFailure(error): Action {
    return {
      type: CLUB_ACTION_TYPES.CLUB_GET_FAILURE,
      error,
    } as Action
  }
  public getReset(): Action {
    return { type: CLUB_ACTION_TYPES.CLUB_GET_RESET } as Action
  }
  public createRequest(): Action {
    return { type: CLUB_ACTION_TYPES.CLUB_CREATE_REQUEST } as Action
  }
  public createSuccess(club: Club): Action {
    return {
      type: CLUB_ACTION_TYPES.CLUB_CREATE_SUCCESS,
      club,
    } as Action
  }
  public createFailure(error): Action {
    return {
      type: CLUB_ACTION_TYPES.CLUB_CREATE_FAILURE,
      error,
    } as Action
  }
  public createReset(): Action {
    return { type: CLUB_ACTION_TYPES.CLUB_CREATE_RESET } as Action
  }
}
